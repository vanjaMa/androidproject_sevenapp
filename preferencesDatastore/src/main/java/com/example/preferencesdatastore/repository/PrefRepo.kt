package com.example.preferencesdatastore.repository

import kotlinx.coroutines.flow.Flow

interface PrefRepo {
    fun getUsername() : Flow<String?>

    suspend fun setUsername(username : String)

}