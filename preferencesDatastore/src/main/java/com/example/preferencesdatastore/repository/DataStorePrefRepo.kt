package com.example.preferencesdatastore.repository

import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.Preferences
import androidx.datastore.preferences.core.edit
import com.example.preferencesdatastore.keys.PreferencesProperties
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.map
import javax.inject.Inject

class DataStorePrefRepo @Inject constructor(
     private val prefDataStore: DataStore<Preferences>
) : PrefRepo {

     override fun getUsername(): Flow<String?> {
          return prefDataStore.data
               .map { preferences -> preferences[PreferencesProperties.Keys.USERNAME]
               }
     }

     override suspend fun setUsername(username: String) {
          prefDataStore.edit {  preferences -> preferences[PreferencesProperties.Keys.USERNAME] = username
          }
     }
     }
