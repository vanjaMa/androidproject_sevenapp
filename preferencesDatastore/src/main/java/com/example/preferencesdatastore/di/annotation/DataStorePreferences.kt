package com.example.preferencesdatastore.di.annotation

import javax.inject.Qualifier


@Qualifier
@Retention(AnnotationRetention.BINARY)
annotation class DataStorePreferences()
