package com.example.preferencesdatastore.di

import android.content.Context
import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.Preferences
import androidx.datastore.preferences.preferencesDataStore
import com.example.preferencesdatastore.di.annotation.DataStorePreferences
import com.example.preferencesdatastore.keys.PreferencesProperties
import com.example.preferencesdatastore.repository.DataStorePrefRepo
import com.example.preferencesdatastore.repository.PrefRepo
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent

val Context.dataStorePref : DataStore<Preferences> by preferencesDataStore(name = PreferencesProperties.NAME)
@InstallIn(SingletonComponent::class)
@Module
class DataStoreModul {

    @Provides
    fun provideDataStore(@ApplicationContext context: Context) : DataStore<Preferences> =
        context.dataStorePref

    @Provides
    @DataStorePreferences
    fun provideDataStorePrefRepo (dataStore: DataStore<Preferences>) : PrefRepo = DataStorePrefRepo(dataStore)
}