package com.example.preferencesdatastore.keys

import androidx.datastore.preferences.core.stringPreferencesKey

object PreferencesProperties {
    val NAME = "PREFERENCES"

    object Keys {
        val USERNAME = stringPreferencesKey("NAME")
    }
}