package com.com.todoAppseven

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import com.com.todoAppseven.presentation.SevenApp
import com.com.todoAppseven.presentation.helpers.NavigationType
import com.com.todoAppseven.ui.theme.SevenTheme
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : ComponentActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            SevenTheme {
                val navigationType = NavigationType.BOTTOM_NAV
                SevenApp(navigationType = navigationType)
            }
        }
    }
}

