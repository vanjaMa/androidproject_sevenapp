package com.com.todoAppseven.data.repository

import com.com.todoAppseven.data.model.Step
import kotlinx.coroutines.flow.Flow

interface StepRepo {

    fun getAllSteps(): Flow<List<Step>>

    fun getStepById(id: Long): Flow<Step?>

    suspend fun addStep(step: Step) : Long

    suspend fun updateStep(step: Step)

    suspend fun removeStep(step: Step)

    suspend fun removeSteps(vararg steps: Step)
}