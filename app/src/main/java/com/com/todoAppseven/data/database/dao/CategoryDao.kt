package com.com.todoAppseven.data.database.dao

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import androidx.room.Transaction
import androidx.room.Update
import com.com.todoAppseven.data.model.Category
import com.com.todoAppseven.data.model.ColorHolder
import com.com.todoAppseven.data.model.relationships.CategoryMitTodos
import kotlinx.coroutines.flow.Flow
import java.time.LocalDate
//methoden bereitgestellt die meine app verwenden kann

@Dao
interface CategoryDao {
    @Query("SELECT * FROM categories")
    fun getAllCategories(): Flow<List<Category>>

    @Query("SELECT * FROM categories WHERE id = :id")
    fun getCategoryById(id: Long): Flow<Category?>

    @Transaction
    @Query("SELECT * FROM categories")
    fun getAllCategoriesWithTodos(): Flow<List<CategoryMitTodos>>

    @Transaction
    @Query("SELECT * FROM categories WHERE id = :id")
    fun getCategoryWithTodoById(id: Long): Flow<CategoryMitTodos?>

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insertCategory(category: Category) : Long

    @Update(onConflict = OnConflictStrategy.IGNORE)
    suspend fun updateCategory(category: Category)

    @Delete
    suspend fun deleteCategory(category: Category)

    @Delete
    suspend fun deleteCategories(vararg categories: Category)
}
