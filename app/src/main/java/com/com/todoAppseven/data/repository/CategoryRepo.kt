package com.com.todoAppseven.data.repository

import android.graphics.Color
import com.com.todoAppseven.data.model.Category
import com.com.todoAppseven.data.model.ColorHolder
import com.com.todoAppseven.data.model.relationships.CategoryMitTodos
import kotlinx.coroutines.flow.Flow
import java.time.LocalDate

interface CategoryRepo {
    //returns alle categories
    fun getAllCategories(): Flow<List<Category>>

    //returns alle cat mit id
    fun getCategoryById(id: Long): Flow<Category?>

    //gibt alle kategorien mit zugehörigen todos die in categorymittodos eingebettet sind
    fun getAllCategoriesWithTodos(): Flow<List<CategoryMitTodos>>

    //gibt alle zugehörigen todos  mit der id zurück die in categorymitTodos eingebettet ist
    fun getCategoryWithTodoById(id: Long): Flow<CategoryMitTodos?>

    suspend fun addCategory(category: Category) : Long

    suspend fun updateCategory(category: Category)

    //eine category wird entfernt
    suspend fun removeCategory(category: Category)

    //mehrere categorien werden entfernt
    //vararg = variable anzahl von argumenten
    //innerhalb der fun wird vararg wie array behandeltn
    suspend fun removeCategories(vararg categories: Category)

}