package com.com.todoAppseven.data.model.relationships

import androidx.room.Embedded
import androidx.room.Relation
import com.com.todoAppseven.data.model.Category
import com.com.todoAppseven.data.model.Todo

//embedded sagt room dass, es die eigenschaften der Category klasse
//in die gleiche tabelle einfügen soll in der CategoryMitTodos

data class CategoryMitTodos (
    @Embedded val category: Category,
    @Relation(      //definiert eine beziehung zw 2 Entitäten
        parentColumn = "id",            //spalte in categoryTabelle-> als fremdschlüssel in der todo tabelle
        entityColumn = "category_id" //bezieht sich auf die Spalte in toodo tabelle
    )                                   //die den fremdschlüssel enthält
    val todos: List<Todo> //liste von todoo objekten die zu kategorie gehören
)
