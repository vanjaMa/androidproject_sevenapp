package com.com.todoAppseven.data.repository

import com.com.todoAppseven.data.database.dao.TodoDao
import com.com.todoAppseven.data.model.Category
import com.com.todoAppseven.data.model.Todo
import com.com.todoAppseven.data.model.relationships.TodosMitSteps
import kotlinx.coroutines.flow.Flow
import java.time.LocalDate
import javax.inject.Inject

class TodoRepoImpl @Inject constructor(private val dao: TodoDao): TodoRepo{

    override fun getAllTodos(): Flow<List<Todo>> = dao.getAllTodos()

    override fun getAllUncategorizedTodos(): Flow<List<Todo>> = dao.getAllUncategorizedTodos()

    override fun getTodoById(id: Long): Flow<Todo?> = dao.getTodoById(id)

    override fun getAllTodosWithSteps(): Flow<List<TodosMitSteps>> = dao.getAllTodosWithSteps()

    override fun getTodoWithStepsById(id: Long): Flow<TodosMitSteps?> = dao.getTodoWithStepsById(id)

    override  fun getTodosSortedByDate(): Flow<List<Todo>> =  dao.getTodosSortedByDate()

    override suspend fun addTodo(todo: Todo) = dao.insertTodo(todo)

    override suspend fun updateTodo(todo: Todo) = dao.updateTodo(todo)

    override suspend fun removeTodo(todo: Todo) = dao.deleteTodo(todo)

    override suspend fun removeTodos(vararg todos: Todo) = dao.deleteTodos(todos = todos)


}