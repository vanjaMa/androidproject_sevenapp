package com.com.todoAppseven.data.di

import android.content.Context
import androidx.room.Room
import com.com.todoAppseven.data.database.SevenAppDatabase
import com.com.todoAppseven.data.database.dao.CategoryDao
import com.com.todoAppseven.data.database.dao.StepDao
import com.com.todoAppseven.data.database.dao.TodoDao
import com.com.todoAppseven.data.di.annotation.RoomDataSource
import com.com.todoAppseven.data.repository.CategoryRepo
import com.com.todoAppseven.data.repository.CategoryRepoImpl
import com.com.todoAppseven.data.repository.StepRepo
import com.com.todoAppseven.data.repository.StepRepoImpl
import com.com.todoAppseven.data.repository.TodoRepo
import com.com.todoAppseven.data.repository.TodoRepoImpl
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton


//Modul für Bereitstellung vo Datenbank- und Repository-Dependencies deniniert
//
@InstallIn(SingletonComponent::class)
@Module
class RoomDataModul {
    @Provides
    fun provideTodoDao(database: SevenAppDatabase) = database.todoDao()

    @Provides
    fun provideStepDao(database: SevenAppDatabase) = database.stepDao()

    @Provides
    fun provideCategoryDao(database: SevenAppDatabase) = database.categoryDao()

    @Provides
    @RoomDataSource
    fun provideCategoryRepo(dao: CategoryDao) : CategoryRepo = CategoryRepoImpl(dao)

    @Provides
    @RoomDataSource
    fun provideTodoRepo(dao: TodoDao): TodoRepo = TodoRepoImpl(dao)

    @Provides
    @RoomDataSource
    fun provideStepRepo(dao: StepDao): StepRepo = StepRepoImpl(dao)


    @Provides
    @Singleton
    fun provideDatabase(@ApplicationContext context: Context) : SevenAppDatabase {
        return Room.databaseBuilder(
            context = context,
            klass = SevenAppDatabase::class.java,
            "Seven"
        ).build()
    }
}