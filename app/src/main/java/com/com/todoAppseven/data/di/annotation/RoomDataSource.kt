package com.com.todoAppseven.data.di.annotation

import javax.inject.Qualifier

//annotation
//ist wie ein spezielles Etikett oder eine Notiz,
// die man an einen Teil vom Codes anhängen kann,
// um zusätzliche Informationen zu geben oder das Verhalten des Codes zu ändern
//  wird oft verwendet, um Tools und Frameworks zu sagen wie sie mit dem Code umgehen sollen

// ist wie das Hinzufügen einer speziellen Anweisung oder eines Hinweises zum Code,
// der hilft, bestimmte Dinge automatisch zu erledigen

@Qualifier
@Retention(AnnotationRetention.BINARY)
annotation class RoomDataSource


