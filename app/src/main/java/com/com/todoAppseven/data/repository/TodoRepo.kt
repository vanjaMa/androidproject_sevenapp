package com.com.todoAppseven.data.repository

import com.com.todoAppseven.data.model.Category
import com.com.todoAppseven.data.model.Todo
import com.com.todoAppseven.data.model.relationships.TodosMitSteps
import kotlinx.coroutines.flow.Flow
import java.time.LocalDate

interface TodoRepo {

    fun getAllTodos(): Flow<List<Todo>>


    fun getAllUncategorizedTodos(): Flow<List<Todo>>

    fun getTodoById(id: Long): Flow<Todo?>


    fun getAllTodosWithSteps(): Flow<List<TodosMitSteps>>


    fun getTodoWithStepsById(id: Long): Flow<TodosMitSteps?>

     fun getTodosSortedByDate(): Flow<List<Todo>>

    suspend fun addTodo(todo: Todo) : Long

    suspend fun updateTodo(todo: Todo)


    suspend fun removeTodo(todo: Todo)


    suspend fun removeTodos(vararg todos: Todo)



}
