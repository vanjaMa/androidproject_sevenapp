package com.com.todoAppseven.data.model.relationships

import androidx.room.Embedded
import androidx.room.Relation
import com.com.todoAppseven.data.model.Step
import com.com.todoAppseven.data.model.Todo

data class TodosMitSteps(
    @Embedded val todo: Todo,
    @Relation(
        parentColumn = "id",
        entityColumn = "todo_id"
    )
    val steps: List<Step>
)
