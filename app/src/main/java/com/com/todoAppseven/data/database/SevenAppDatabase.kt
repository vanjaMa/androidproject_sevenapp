package com.com.todoAppseven.data.database

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.com.todoAppseven.data.database.converter.LocalDateConverter
import com.com.todoAppseven.data.database.converter.LocalDateTimeConverter
import com.com.todoAppseven.data.database.dao.CategoryDao
import com.com.todoAppseven.data.database.dao.StepDao
import com.com.todoAppseven.data.database.dao.TodoDao
import com.com.todoAppseven.data.model.Category
import com.com.todoAppseven.data.model.Step
import com.com.todoAppseven.data.model.Todo

//datenklasse die die datenbank hält
//stellt für die app instanzen der daoS die mit der datenbank verbunden sind
@Database(
    entities = [Todo::class, Category::class, Step::class],exportSchema = false,
    version = 1
)
@TypeConverters(value = [LocalDateConverter::class, LocalDateTimeConverter::class])
abstract class SevenAppDatabase: RoomDatabase() {
    abstract fun todoDao(): TodoDao
    abstract fun stepDao(): StepDao
    abstract fun categoryDao(): CategoryDao
}
