package com.com.todoAppseven.data.database.dao

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import androidx.room.Transaction
import androidx.room.Update
import com.com.todoAppseven.data.model.Todo
import com.com.todoAppseven.data.model.relationships.TodosMitSteps
import kotlinx.coroutines.flow.Flow
import java.time.LocalDate


@Dao
interface TodoDao {
    @Query("SELECT * FROM todos")
    fun getAllTodos(): Flow<List<Todo>>

    @Query("SELECT * FROM todos WHERE category_id IS NULL")
    fun getAllUncategorizedTodos(): Flow<List<Todo>>

    @Query("SELECT * FROM todos WHERE id = :id")
    fun getTodoById(id: Long): Flow<Todo?>

    @Transaction
    @Query("SELECT * FROM todos")
    fun getAllTodosWithSteps(): Flow<List<TodosMitSteps>>

    @Transaction
    @Query("SELECT * FROM todos WHERE id = :id")
    fun getTodoWithStepsById(id: Long): Flow<TodosMitSteps?>

    @Transaction
    @Query("SELECT * FROM todos ORDER BY due_date ASC")
    fun getTodosSortedByDate(): Flow<List<Todo>>

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insertTodo(todo: Todo) : Long

    @Update(onConflict = OnConflictStrategy.IGNORE)
    suspend fun updateTodo(todo: Todo)

    @Delete
    suspend fun deleteTodo(todo: Todo)

    @Delete
    suspend fun deleteTodos(vararg todos: Todo)



}











