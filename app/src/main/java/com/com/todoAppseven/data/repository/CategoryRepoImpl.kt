package com.com.todoAppseven.data.repository
import android.graphics.Color
import com.com.todoAppseven.data.database.dao.CategoryDao
import com.com.todoAppseven.data.model.Category
import com.com.todoAppseven.data.model.ColorHolder
import com.com.todoAppseven.data.model.relationships.CategoryMitTodos
import kotlinx.coroutines.flow.Flow
import java.time.LocalDate
import javax.inject.Inject

class CategoryRepoImpl @Inject constructor(private val dao: CategoryDao): CategoryRepo {

    override fun getAllCategories(): Flow<List<Category>> = dao.getAllCategories()

    override fun getCategoryById(id: Long): Flow<Category?> = dao.getCategoryById(id)

    override fun getAllCategoriesWithTodos(): Flow<List<CategoryMitTodos>> = dao.getAllCategoriesWithTodos()

    override fun getCategoryWithTodoById(id: Long): Flow<CategoryMitTodos?> = dao.getCategoryWithTodoById(id)

    override suspend fun addCategory(category: Category) = dao.insertCategory(category)

    override suspend fun updateCategory(category: Category) = dao.updateCategory(category)

    override suspend fun removeCategory(category: Category) = dao.deleteCategory(category)

    override suspend fun removeCategories(vararg categories: Category) = dao.deleteCategories(categories = categories)

}