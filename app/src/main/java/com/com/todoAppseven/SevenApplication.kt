package com.com.todoAppseven

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

//initialisiert Hilt DI-Framework
//quasi wie ein werkzeugkasten für ein Bauprojekt
// wenn ein teil vom code ein werkzeug braucht geht es einfach zu  dagger hilt und holt es sich
//->code sauberer, einfacher zu verstehen und zu warten
//di library-> stück code, das ein anderes stück code braucht,  dagger hilt vereinfacht das
@HiltAndroidApp
class SevenApplication: Application(){
}

