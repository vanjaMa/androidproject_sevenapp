package com.com.todoAppseven.presentation.view.screens.pagerNav

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.twotone.Category
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.lifecycle.viewmodel.compose.viewModel
import com.com.todoAppseven.R
import com.com.todoAppseven.data.model.Category
import com.com.todoAppseven.data.model.Todo
import com.com.todoAppseven.presentation.helpers.NavDest
import com.com.todoAppseven.presentation.helpers.getCompletedTodosRatio
import com.com.todoAppseven.presentation.viewmodel.CategoriesScreenUiState
import com.com.todoAppseven.presentation.viewmodel.CategoriesScreenViewModel
import com.com.todoAppseven.ui.theme.spacing

object CategoriesScreenDestination : NavDest(
    displayedTitle = R.string.categories,
    route = "categories",
    Icons.TwoTone.Category
) {
    override val route: String = super.route
    const val parameterName = "categoryId"
//
//    val parametrizedRoute = "$route/{$parameterName}"
//    fun destinationWithParam(categoryId: Long) = "$route/$categoryId"

}


@Composable
fun CategoriesScreen(
    uiState: CategoriesScreenUiState,
   //modifier: Modifier = Modifier,
    onCategoryClick: (Category) -> Unit = {},
    onAddCategoryClick: () -> Unit = {},
    onTodoClick: (Todo) -> Unit = {},
) {
    val viewModel = hiltViewModel<CategoriesScreenViewModel>()
    val uiState by viewModel.uiState.collectAsState()


    Surface(
        modifier = Modifier
            .fillMaxWidth(),

    ) {
        LazyColumn(
           verticalArrangement = Arrangement.spacedBy (MaterialTheme.spacing.small, Alignment.CenterVertically),
            horizontalAlignment = Alignment.CenterHorizontally,
            ) {
            item {
                AddCategoryTile(onClick = onAddCategoryClick)
            }
            items(uiState.categoryList) {
                CategoryTile(

                    onClick = onCategoryClick,
                    category = it.category,
                    progress = it.getCompletedTodosRatio(),
                    modifier = Modifier.fillMaxWidth()
                        .padding(horizontal = MaterialTheme.spacing.small)
                        .padding(bottom = MaterialTheme.spacing.small),
                )
            }
        }
    }
}



