package com.com.todoAppseven.presentation.view.components

import android.widget.Toast
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.twotone.Check
import androidx.compose.material.icons.twotone.Delete
import androidx.compose.material3.AlertDialog
import androidx.compose.material3.Card
import androidx.compose.material3.Checkbox
import androidx.compose.material3.DatePickerDialog
import androidx.compose.material3.DatePickerState
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.OutlinedTextField
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.material3.TextButton
import androidx.compose.material3.rememberDatePickerState
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.dp
import com.com.todoAppseven.R
import com.com.todoAppseven.data.model.Step
import com.com.todoAppseven.data.model.Todo
import com.com.todoAppseven.presentation.viewmodel.exceptions.InvalidDateException
import com.com.todoAppseven.ui.theme.spacing
import java.time.Instant
import java.time.LocalDate
import java.time.ZoneId
import java.time.format.DateTimeFormatter


@Composable
 internal fun TodoTile(
    onClick: (Todo) -> Unit,
    onCheckboxClick: (Todo, Boolean) -> Unit,
    todo: Todo,
    modifier: Modifier = Modifier
) {
    Surface(
        modifier = modifier
            .fillMaxWidth(),
        onClick = {
            onClick(todo)
        },
        shape = MaterialTheme.shapes.medium,
        shadowElevation = 12.dp
    ) {
        Row(
            modifier = Modifier.padding(MaterialTheme.spacing.small),
            verticalAlignment = Alignment.CenterVertically
        ) {
            Checkbox(
                checked = todo.isDone,
                onCheckedChange = {
                    onCheckboxClick(todo, it)
                }
            )
            Text(
                modifier = Modifier.weight(3F),
                text = todo.name,
                maxLines = 1,
                overflow = TextOverflow.Ellipsis
            )
            Text(
                modifier = Modifier.weight(1F),
                text = todo.dueDate.format(DateTimeFormatter.ofPattern("E, dd MMM yyyy")),
                style = MaterialTheme.typography.labelSmall,
                maxLines = 1,
                overflow = TextOverflow.Ellipsis
            )
        }
    }
}

@Composable
 fun ConfirmationDialog(
    onDismissRequest: () -> Unit,
    onConfirmation: () -> Unit,
    dialogTitle: String,
    dialogText: String,
    icon: ImageVector,
) {
    AlertDialog(
        icon = {
            Icon(icon, contentDescription = null)
        },
        title = {
            Text(text = dialogTitle)
        },
        text = {
            Text(text = dialogText)
        },
        onDismissRequest = {
            onDismissRequest()
        },
        confirmButton = {
            TextButton(
                onClick = {
                    onConfirmation()
                }
            ) {
                Text(stringResource(R.string.bestaetigen)) //.uppercase
            }
        },
        dismissButton = {
            TextButton(
                onClick = {
                    onDismissRequest()
                }
            ) {
                Text(stringResource(R.string.abbrechen))
            }
        }
    )
}
@Composable
 fun InputField(
    label: String,
    value: String,
    onValueChanged: (String) -> Unit,
    modifier: Modifier = Modifier,
    maxLines: Int = 1,
    singleLine:Boolean = true,
    readOnly: Boolean = false,
    trailingIcon: @Composable (() -> Unit) = {}
) {
    OutlinedTextField(
        modifier = modifier,
        value = value,
        onValueChange = onValueChanged,
        label = {
            Text(text = label)
        },
        maxLines = maxLines,
        shape = MaterialTheme.shapes.medium,
        singleLine = singleLine,
        readOnly = readOnly,
        trailingIcon = trailingIcon
    )
}

@Composable
 fun StepTile(
    step: Step,
    onStepChecked: (Step, Boolean) -> Unit,
    onRemoveClick: (Step) -> Unit,
    editMode: Boolean,
    modifier: Modifier = Modifier
) {
    Card(
        modifier = modifier
            .fillMaxWidth(),
        shape = MaterialTheme.shapes.medium
    ) {
        Row(
            modifier = Modifier.padding(MaterialTheme.spacing.small),
            verticalAlignment = Alignment.CenterVertically
        ) {
            if(!editMode) {
                Checkbox(
                    checked = step.isDone,
                    onCheckedChange = {
                        onStepChecked(step, it)
                    }
                )
            }
            Text(
                text = step.description,
                maxLines = 1,
                overflow = TextOverflow.Ellipsis,
                modifier = modifier.padding(start = if(editMode) MaterialTheme.spacing.small else MaterialTheme.spacing.default)
            )
            if(editMode && step.isDone) Icon(imageVector = Icons.TwoTone.Check, contentDescription = null)
            Spacer(modifier = Modifier.weight(1F))
            if(editMode) {
                IconButton(
                    onClick = { onRemoveClick(step) }
                ) {
                    Icon(imageVector = Icons.TwoTone.Delete, contentDescription = null)
                }
            }
        }
    }
}

@Composable
@OptIn(ExperimentalMaterial3Api::class)
 fun DatePicker(
    onEntryValueChanged: (LocalDate) -> Unit,
    onDismissRequest: () -> Unit,
    title: String,
    headline: String,
    confirmEnabled: Boolean,
    confirmButtonText: String,
    dismissButtonText: String,
    datePickerState: DatePickerState = rememberDatePickerState()
) {
    DatePickerDialog(
        onDismissRequest = onDismissRequest,
        confirmButton = {
            val context = LocalContext.current
            TextButton(
                onClick =  {
                    try {
                        val selectedDate = Instant
                            .ofEpochMilli(datePickerState.selectedDateMillis ?: 0L)
                            .atZone(ZoneId.systemDefault())
                            .toLocalDate()
                        onEntryValueChanged(selectedDate)
                        onDismissRequest()
                    } catch (e: InvalidDateException) {
                        Toast.makeText(context, context.getText(e.displayMessage), Toast.LENGTH_SHORT).show()
                    }
                },
                enabled = confirmEnabled
            ) {
                Text(confirmButtonText.uppercase())
            }
        },
        dismissButton = {
            TextButton(
                onClick = onDismissRequest
            ) {
                Text(dismissButtonText.uppercase())
            }
        }
    ) {
        androidx.compose.material3.DatePicker(
            title = {
                Text(
                    text = title,
                    modifier = Modifier.padding(8.dp)
                )
            },
            headline = {
                Text(
                    text = headline,
                    modifier = Modifier.padding(8.dp)
                )
            },
            state = datePickerState,
            showModeToggle = false
        )
    }
}
