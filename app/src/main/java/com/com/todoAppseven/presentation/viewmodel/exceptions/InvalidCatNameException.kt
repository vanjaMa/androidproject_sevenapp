package com.com.todoAppseven.presentation.viewmodel.exceptions

import com.com.todoAppseven.R

class InvalidCatNameException (
    override val displayMessage: Int = R.string.catNameException
): SevenAppException("Der Kategoriename darf nicht leer sein!")