package com.com.todoAppseven.presentation.view.screens.pagerNav

import androidx.compose.animation.AnimatedContent
import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import com.com.todoAppseven.R
import com.com.todoAppseven.presentation.helpers.NavDest
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.twotone.CalendarMonth
import androidx.compose.material3.Checkbox
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import com.com.todoAppseven.data.model.Todo
import com.com.todoAppseven.presentation.view.components.TodoTile
import com.com.todoAppseven.presentation.view.screens.TodoDisplayDestination
import com.com.todoAppseven.presentation.viewmodel.CalendarScreenUiState
import com.com.todoAppseven.presentation.viewmodel.CalendarScreenViewModel
import com.com.todoAppseven.ui.theme.spacing
import java.time.format.DateTimeFormatter


object CalendarDestination : NavDest(
    displayedTitle = R.string.calender,
    route = "calender",
    Icons.TwoTone.CalendarMonth
) {
    override val route: String = super.route
    const val parameterName = "stepId"

    val parametrizedRoute = "${TodoDisplayDestination.route}/{$parameterName}"
    fun destinationWithParam(todoId: Long) = "${TodoDisplayDestination.route}/$todoId"

}

@OptIn(ExperimentalFoundationApi::class)
@Composable
fun CalendarScreen(
    uiState: CalendarScreenUiState,
    modifier: Modifier = Modifier,
    onTodoClick: (Todo) -> Unit,
    onTodoCheckedChanged: (Todo, Boolean) -> Unit,
) {
    val viewModel = hiltViewModel<CalendarScreenViewModel>()
    val uiState by viewModel.uiState.collectAsState()

    Surface(modifier = modifier.fillMaxSize()) {
        LazyColumn(contentPadding = PaddingValues(vertical = MaterialTheme.spacing.small)) {
            stickyHeader {
                Text(
                    text = stringResource(R.string.today_todos), //.uppercase()
                    style = MaterialTheme.typography.labelMedium,
                    modifier = Modifier
                        .padding(start = MaterialTheme.spacing.small)
                        .padding(bottom = MaterialTheme.spacing.small)
                )
            }

                items(uiState.todaysTodos) { todo ->
                    AnimatedContent(
                        targetState = !todo.isDone, label = "Todo done animation"
                    ) {
                        if (it) {
                            TodoTile(
                                modifier = Modifier
                                    .padding(horizontal = MaterialTheme.spacing.small)
                                    .padding(bottom = MaterialTheme.spacing.small),
                                onClick = onTodoClick,
                                onCheckboxClick = onTodoCheckedChanged,
                                todo = todo
                            )
                        }
                    }
                }


            stickyHeader {
                Text(
                    text = stringResource(R.string.upcoming_todos),
                    style = MaterialTheme.typography.labelMedium,
                    modifier = Modifier
                        .padding(start = MaterialTheme.spacing.small)
                        .padding(bottom = MaterialTheme.spacing.small)
                )
            }
            if (uiState.sortedTodosList.isNotEmpty()) {
                items(uiState.sortedTodosList) { todo ->
                    AnimatedContent(targetState = !todo.isDone, label = "Todo done animation") {
                        if (it) {
                            TodoTile(
                                modifier = Modifier
                                    .padding(horizontal = MaterialTheme.spacing.small)
                                    .padding(bottom = MaterialTheme.spacing.small),
                                onClick = onTodoClick,
                                onCheckboxClick = onTodoCheckedChanged,
                                todo = todo
                            )
                        }
                    }
                }
            }
            }
        }
    }



