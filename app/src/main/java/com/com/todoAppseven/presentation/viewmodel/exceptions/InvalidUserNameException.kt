package com.com.todoAppseven.presentation.viewmodel.exceptions

import com.com.todoAppseven.R

class InvalidUserNameException(
    override val displayMessage: Int = R.string.username_empty_warning
) : SevenAppException(message = "Username darf nicht leer sein!") {
}