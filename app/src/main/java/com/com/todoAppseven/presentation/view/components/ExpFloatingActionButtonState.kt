package com.com.todoAppseven.presentation.view.components

import androidx.compose.runtime.Composable
import androidx.compose.runtime.Stable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue

@Composable
fun rememberExpandableFloatingActionButtonState(
    isFabExpanded: Boolean = false
) = remember {
    ExpFloatingActionButtonState(isFabExpanded)
}
@Stable
class ExpFloatingActionButtonState constructor(
    expanded: Boolean = false
){
    private var isFabExpanded by mutableStateOf(expanded)
    fun expand ( ){isFabExpanded = true}

    fun collapse(){isFabExpanded = false}

    fun isExpanded()= isFabExpanded

}