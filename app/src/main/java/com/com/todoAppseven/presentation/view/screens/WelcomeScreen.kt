package com.com.todoAppseven.presentation.view.screens

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.twotone.ArrowForward
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.OutlinedIconButton
import androidx.compose.material3.OutlinedTextField
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.input.KeyboardCapitalization
import androidx.compose.ui.text.input.KeyboardType
import com.com.todoAppseven.R
import com.com.todoAppseven.presentation.viewmodel.WelcomeScreenUIState
import com.com.todoAppseven.ui.theme.spacing
import com.com.todoAppseven.presentation.viewmodel.isValid


@Composable
fun WelcomeScreen(
    uiState: WelcomeScreenUIState,
    onInputFieldValueChange: (String)->Unit,
    onSaveClick: ()->Unit,
    modifier: Modifier= Modifier
) {

    Surface(modifier= modifier.fillMaxSize()
    ) {
        Column (
            verticalArrangement = Arrangement.spacedBy(MaterialTheme.spacing.medium, Alignment.CenterVertically),
            horizontalAlignment = Alignment.CenterHorizontally,
            modifier = Modifier
                .fillMaxSize()
        ){
            Text(
                text =  stringResource (R.string.welcome_screen_quest),
                style = MaterialTheme.typography.headlineSmall
            )
            OutlinedTextField(
                value = uiState.username ?: "",
                onValueChange = onInputFieldValueChange,
                label = {
                    Text(text = stringResource(R.string.username))
                },
                singleLine = true,
                keyboardOptions = KeyboardOptions(
                    capitalization = KeyboardCapitalization.Words,
                    keyboardType = KeyboardType.Text,
                    imeAction = ImeAction.Go
                ),
                keyboardActions = KeyboardActions(
                    onGo = {
                        onSaveClick()

                    }
                )
            )
            OutlinedIconButton(
                modifier = Modifier,
                enabled = uiState.isValid(),
                onClick = onSaveClick
            ) {
                Icon(
                    imageVector = Icons.TwoTone.ArrowForward,
                    contentDescription = null
                )
            }
        }
    }
}