package com.com.todoAppseven.presentation.view.screens.pagerNav

import androidx.compose.animation.AnimatedContent
import androidx.compose.animation.core.animateFloatAsState
import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.LazyRow
import androidx.compose.foundation.lazy.items
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.twotone.Add
import androidx.compose.material.icons.twotone.Home
import androidx.compose.material.icons.twotone.ThumbUp
import androidx.compose.material3.Icon
import androidx.compose.material3.LinearProgressIndicator
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.material3.TextButton
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.dp
import com.com.todoAppseven.R
import com.com.todoAppseven.data.model.Category
import com.com.todoAppseven.data.model.Todo
import com.com.todoAppseven.presentation.helpers.NavDest
import com.com.todoAppseven.presentation.helpers.getCompletedTodosRatio
import com.com.todoAppseven.presentation.helpers.progress
import com.com.todoAppseven.presentation.helpers.toComposeColor
import com.com.todoAppseven.presentation.view.components.TodoTile
import com.com.todoAppseven.presentation.viewmodel.DashBoardScreenUiState
import com.com.todoAppseven.ui.theme.spacing

object DashboardDestination : NavDest(
    displayedTitle = R.string.dashboard,
    route = "dashboard",
    Icons.TwoTone.Home
)

@OptIn(ExperimentalFoundationApi::class)
@Composable
fun DashboardScreen(
    uiState: DashBoardScreenUiState,
    modifier: Modifier = Modifier,
    onCategoryClick: (Category) -> Unit = {},
    onAddCategoryClick: () -> Unit = {},
    onTodoClick: (Todo) -> Unit,
    onTodoCheckedChanged: (Todo, Boolean) -> Unit = { _, _ -> }
    ){
    Surface(
        modifier = modifier.fillMaxSize()
    ) {
        LazyColumn(
            contentPadding = PaddingValues(vertical = MaterialTheme.spacing.small)
        ) {
            stickyHeader {
                Text(
                    text = stringResource(R.string.categories), //.uppercase()
                    style = MaterialTheme.typography.labelMedium,
                    modifier = Modifier
                        .padding(start = MaterialTheme.spacing.small)
                        .padding(bottom = MaterialTheme.spacing.small)
                )
            }
            item {
                LazyRow(
                    contentPadding = PaddingValues(horizontal = MaterialTheme.spacing.small),
                    horizontalArrangement = Arrangement.spacedBy(MaterialTheme.spacing.small),
                    modifier = Modifier.padding(bottom = MaterialTheme.spacing.small)
                ) {
                    items(uiState.categoryList) {
                        CategoryTile(
                            onClick = onCategoryClick,
                            category = it.category,
                            progress = it.getCompletedTodosRatio()
                        )
                    }
                    item {
                        CategoryTile(
                            onClick = onCategoryClick,
                            category = Category.uncategorizedCategory(context = LocalContext.current),
                            progress = uiState.uncategorizedTodos.progress())
                    }
                    item {
                        AddCategoryTile(onClick = onAddCategoryClick)
                    }
                }
            }
            stickyHeader {
                Text(
                    text = stringResource(R.string.today_todos), //.uppercase()
                    style = MaterialTheme.typography.labelMedium,
                    modifier = Modifier
                        .padding(start = MaterialTheme.spacing.small)
                        .padding(bottom = MaterialTheme.spacing.small)
                )
            }
            if (uiState.todaysTodos.isEmpty()) {
                item {
                    CongratsTile()
                }
            } else {
                items(uiState.todaysTodos) { todo ->
                    AnimatedContent(
                        targetState = !todo.isDone, label = "Todo done animation"
                    ) {
                        if (it) {
                            TodoTile(
                                modifier = Modifier
                                    .padding(horizontal = MaterialTheme.spacing.small)
                                    .padding(bottom = MaterialTheme.spacing.small),
                                onClick = onTodoClick,
                                onCheckboxClick = onTodoCheckedChanged,
                                todo = todo
                            )
                        }
                    }
                }
            }
        }
    }
}

@Composable
fun CategoryTile(
    onClick: (Category) -> Unit,
    category: Category,
    progress: Float,
    modifier: Modifier = Modifier,
) {
    Surface(
        modifier = modifier
            .size(
                width = 220.dp,
                height = 120.dp
            ),
        onClick = {
            onClick(category)
        },
        shape = MaterialTheme.shapes.medium,
        shadowElevation = 12.dp
    ) {
        Column(
            modifier = Modifier
                .padding(
                    horizontal = MaterialTheme.spacing.medium,
                    vertical = MaterialTheme.spacing.large
                )
                .fillMaxSize(),
            horizontalAlignment = Alignment.Start, verticalArrangement = Arrangement.SpaceBetween
        ) {
            val animatedProgress by animateFloatAsState(targetValue = progress, label = "progress value animation")
            Text(text = category.name, maxLines = 1, overflow = TextOverflow.Ellipsis)
            LinearProgressIndicator( //progressLinie
                progress = { animatedProgress },
                color = category.color.toComposeColor(),
            )
        }
    }
}

@Composable
fun AddCategoryTile(
    onClick: () -> Unit,
    modifier: Modifier = Modifier
) {
    Surface(
        modifier = modifier
            .size(
                width = 220.dp,
                height = 120.dp
            ),
        shape = MaterialTheme.shapes.medium
    ) {
        TextButton(
            onClick = onClick,
            shape = MaterialTheme.shapes.medium
        ) {
            Column(
                verticalArrangement = Arrangement.spacedBy(
                    MaterialTheme.spacing.small,
                    Alignment.CenterVertically
                ),
                horizontalAlignment = Alignment.CenterHorizontally
            ) {
                Icon(imageVector = Icons.TwoTone.Add, contentDescription = null)
                Text(
                    text = stringResource(R.string.add_new_category),//.uppercase()
                    textAlign = TextAlign.Center
                )
            }
        }
    }
}
@Composable
fun CongratsTile(
    modifier: Modifier = Modifier
) {
    Column(
        horizontalAlignment = Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.spacedBy(MaterialTheme.spacing.medium),
        modifier = modifier.fillMaxSize()
    ) {
        Icon(
            imageVector = Icons.TwoTone.ThumbUp,
            contentDescription = null,
            tint = MaterialTheme.colorScheme.surfaceTint
        )
        Text(text = stringResource(R.string.congrats), color = MaterialTheme.colorScheme.surfaceTint)
    }
}
