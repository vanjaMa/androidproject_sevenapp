package com.com.todoAppseven.presentation

import android.app.Activity
import android.widget.Toast
import androidx.activity.compose.BackHandler
import androidx.compose.animation.AnimatedVisibility
import androidx.compose.animation.core.animateFloatAsState
import androidx.compose.animation.core.tween
import androidx.compose.animation.scaleIn
import androidx.compose.animation.scaleOut
import androidx.compose.animation.slideInHorizontally
import androidx.compose.animation.slideOutHorizontally
import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.pager.HorizontalPager
import androidx.compose.foundation.pager.PagerState
import androidx.compose.foundation.pager.rememberPagerState
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.twotone.Add
import androidx.compose.material.icons.twotone.AddTask
import androidx.compose.material.icons.twotone.Android
import androidx.compose.material.icons.twotone.ArrowBack
import androidx.compose.material.icons.twotone.Category
import androidx.compose.material3.CenterAlignedTopAppBar
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.NavigationBar
import androidx.compose.material3.NavigationBarItem
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableIntStateOf
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.rotate
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.LocalFocusManager
import androidx.compose.ui.res.stringResource
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavHostController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.rememberNavController
import com.com.todoAppseven.R
import com.com.todoAppseven.presentation.helpers.NavDest
import com.com.todoAppseven.presentation.helpers.NavigationType
import com.com.todoAppseven.presentation.helpers.getPagerDestinationsList
import com.com.todoAppseven.presentation.view.components.ExpFloatingActionButtonState
import com.com.todoAppseven.presentation.view.components.SevenFab
import com.com.todoAppseven.presentation.view.components.rememberExpandableFloatingActionButtonState
import com.com.todoAppseven.presentation.view.navigation.sevenAppNavGraph
import com.com.todoAppseven.presentation.view.screens.CategoryDisplayDestination
import com.com.todoAppseven.presentation.view.screens.CategoryDisplayScreen
import com.com.todoAppseven.presentation.view.screens.CategoryEntryDestination
import com.com.todoAppseven.presentation.view.screens.CategoryEntryScreen
import com.com.todoAppseven.presentation.view.screens.TodoDisplayDestination
import com.com.todoAppseven.presentation.view.screens.TodoDisplayScreen
import com.com.todoAppseven.presentation.view.screens.TodoEntryDestination
import com.com.todoAppseven.presentation.view.screens.TodoEntryScreen
import com.com.todoAppseven.presentation.view.screens.WelcomeScreen
import com.com.todoAppseven.presentation.view.screens.pagerNav.CalendarScreen
import com.com.todoAppseven.presentation.view.screens.pagerNav.CategoriesScreen
import com.com.todoAppseven.presentation.view.screens.pagerNav.DashboardScreen
import com.com.todoAppseven.presentation.view.screens.pagerNav.SettingsScreen
import com.com.todoAppseven.presentation.viewmodel.CalendarScreenViewModel
import com.com.todoAppseven.presentation.viewmodel.CategoriesScreenViewModel
import com.com.todoAppseven.presentation.viewmodel.DashboardScreenViewModel
import com.com.todoAppseven.presentation.viewmodel.SettingsScreenViewModel
import com.com.todoAppseven.presentation.viewmodel.SevenAppViewModel
import com.com.todoAppseven.presentation.viewmodel.WelcomeScreenViewModel
import com.com.todoAppseven.presentation.viewmodel.exceptions.InvalidUserNameException
import kotlinx.coroutines.launch


data object EntryDestination : NavDest(
    displayedTitle = 0,
    route = "entry"
)

@OptIn(ExperimentalFoundationApi::class)
@Composable
fun SevenApp(
    navigationType: NavigationType,
    modifier: Modifier = Modifier
) {
    val viewModel: SevenAppViewModel = viewModel()
    val sevenAppState by viewModel.state.collectAsState()


    if (sevenAppState.shouldDisplayWelcomeScreen) {
        val welcomeScreenViewModel: WelcomeScreenViewModel = viewModel()
       val welcomeScreenUIState by welcomeScreenViewModel.uiState.collectAsState()
     val context = LocalContext.current // infos über den aktuellen zustand
        val focusManager = LocalFocusManager.current //bestimmt welches element mit UI und User

        WelcomeScreen(
            uiState = welcomeScreenUIState,
            onInputFieldValueChange = welcomeScreenViewModel::updateUsername,
            onSaveClick = {
                try {
                    welcomeScreenViewModel.saveUsername()
                    focusManager.clearFocus()
                } catch (e: InvalidUserNameException) {
                    Toast.makeText(context, context.getText(e.displayMessage), Toast.LENGTH_SHORT).show()
                }
            },
            modifier = modifier
        )
    } else {
        var currentDestination by rememberSaveable { mutableIntStateOf(0) }
        val navDestination = getPagerDestinationsList()
        val actionButtonState = rememberExpandableFloatingActionButtonState()
        val pagerState = rememberPagerState { navDestination.size }
        val animationScope = rememberCoroutineScope()
        val navController = rememberNavController()


        when (navigationType) {
            NavigationType.BOTTOM_NAV -> {
                BottomNavigationScreen(
                    navController = navController,
                    modifier = modifier,
                    username = sevenAppState.username,
                    currentDestination = currentDestination,
                    navigationDestinations = navDestination,
                    actionButtonState = actionButtonState,
                    pagerState = pagerState,
                    onNavigationItemClick = {
                        currentDestination = it
                        actionButtonState.collapse()
                        animationScope.launch {
                            pagerState.animateScrollToPage(it)
                        }
                    },
                    onNavigateUp = {
                        navController.navigateUp()
                    },
                    onNavigateToRoot = {
                        navController.popBackStack(EntryDestination.route, false)
                    },
                    todoEditClick = {
                        navController.navigate(TodoEntryDestination.destinationWithParam(it))
                    },
                    onCategoryEditClick = {
                        navController.navigate(CategoryEntryDestination.destinationWithParam(it))
                    },
                    onTodoClick = {
                        navController.navigate(TodoDisplayDestination.destinationWithParam(it))
                    }
                )
            }
        }
    }
}
@OptIn(ExperimentalFoundationApi::class)
@Composable
private fun BottomNavigationScreen(
    navController: NavHostController,
    username: String,
    currentDestination: Int,
    navigationDestinations: List<NavDest>,
    pagerState: PagerState,
    modifier: Modifier = Modifier,
    actionButtonState: ExpFloatingActionButtonState = rememberExpandableFloatingActionButtonState(),
    onNavigationItemClick: (Int) -> Unit = {},
    onNavigateUp: () -> Unit,
    onNavigateToRoot: () -> Unit,
    todoEditClick: (Long) -> Unit,
    onCategoryEditClick: (Long) -> Unit,
    onTodoClick: (Long) -> Unit
) {
    NavHost(
        navController = navController,
        startDestination = EntryDestination.route,
        enterTransition = { slideInHorizontally(tween(150)) { it * 2 } },
        exitTransition = { slideOutHorizontally(tween(150)) { it * 2 } },
    ) {
        sevenAppNavGraph(
            entryContent = {
                BottomNavigationScreenContent(
                    navController,
                    modifier,
                    currentDestination,
                    actionButtonState,
                    username,
                    navigationDestinations,
                    onNavigationItemClick,
                    pagerState
                )
            },
            categoryEntryContent = {
                CategoryEntryScreen(
                    onNavigateToRoot = onNavigateToRoot,
                    onNavigateUp = onNavigateUp,
                )
            },
            displayCategoryContent = {
                CategoryDisplayScreen(
                    onEditClick = onCategoryEditClick,
                    onNavigateUp = onNavigateUp,
                    onNavigateToRoot = onNavigateToRoot,
                    onTodoClick = onTodoClick,
                )
            },
            todoEntryContent = {
                TodoEntryScreen(
                    onNavigateUp = onNavigateUp,
                    onNavigateToRoot = onNavigateToRoot
                )
            },
            displayTodoContent = {
                TodoDisplayScreen(
                    onNavigateUp = onNavigateUp,
                    onNavigateToRoot = onNavigateToRoot,
                    onEditClick = todoEditClick,
                )
            }
        )
    }
}

@OptIn(ExperimentalFoundationApi::class, ExperimentalMaterial3Api::class)
@Composable
private fun BottomNavigationScreenContent(
    navController: NavHostController,
    modifier: Modifier,
    currentDestination: Int,
    actionButtonState: ExpFloatingActionButtonState,
    username: String,
    navigationDestinations: List<NavDest>,
    onNavigationItemClick: (Int) -> Unit,
    pagerState: PagerState
) {
    Scaffold(
        modifier = modifier,
        floatingActionButton = {
            AnimatedVisibility( //Floating buttons
                visible = currentDestination != 3,
                enter = scaleIn(tween(150)),
                exit = scaleOut(tween(150))
            ) {
                SevenFab(
                    state = actionButtonState,
                    onFirstActionClick = { navController.navigate(CategoryEntryDestination.route) }, //
                    onSecondActionClick = { navController.navigate(TodoEntryDestination.route) },
                    firstActionContent = {
                        Icon(imageVector = Icons.TwoTone.Category, contentDescription = null)
                    },
                    secondActionContent = {
                        Icon(imageVector = Icons.TwoTone.AddTask, contentDescription = null)
                    }
                ) {
                    val rotation by animateFloatAsState( //drehung vom button
                        targetValue = if (actionButtonState.isExpanded()) 45f else 0f,
                        label = "Expandable Action Button icon rotation"
                    )
                    Icon(
                        imageVector = Icons.TwoTone.Add,
                        contentDescription = null,
                        modifier = Modifier.rotate(rotation)
                    )
                }
            }
        },
        topBar = {
            CenterAlignedTopAppBar( //Begrüßer Bar ganz oben
                title = {
                    Text(                           //stringResources->wartbarkeit & lesbarkeit
                        if (currentDestination == 0) stringResource(R.string.hallo, username)
                        else stringResource(id = navigationDestinations[currentDestination].displayedTitle)
                    )
                },
                navigationIcon = { //zurück pfeil in den anderen pagerscreens außer home
                    AnimatedVisibility(
                        visible = currentDestination != 0,
                        enter = scaleIn(tween(150)),
                        exit = scaleOut(tween(150))
                    ) {
                        IconButton(
                            onClick = {
                                onNavigationItemClick(0)
                            }
                        ) {
                            Icon(imageVector = Icons.TwoTone.ArrowBack, contentDescription = null)
                        }
                    }
                }
            )
        },
        bottomBar = {
            NavigationBar {     //
                navigationDestinations.forEachIndexed { index, item ->
                    NavigationBarItem(
                        icon = {
                            Icon(item.navigationIcon ?: Icons.TwoTone.Android,
                                contentDescription = stringResource(id = item.displayedTitle))
                               },
                        label = { Text(stringResource(id = item.displayedTitle)) },
                        selected = currentDestination == index,
                        onClick = {
                            onNavigationItemClick(index) })
                }
            }
        }
    ) {
        Surface(
            modifier = Modifier.padding(it)
        ) {
            NavigationPager(
                navController,
                pagerState,
                NavigationType.BOTTOM_NAV,
                onBackPressed = { onNavigationItemClick(0) }
        )
        }
    }
}




@OptIn(ExperimentalFoundationApi::class)
@Composable
private fun NavigationPager(
    navController: NavHostController,
    pagerState: PagerState,
    navigationType: NavigationType,
    modifier: Modifier = Modifier,
    onBackPressed: () -> Unit
) {
    when (navigationType) {
        NavigationType.BOTTOM_NAV -> {
            HorizontalPager(
                modifier = modifier,
                state = pagerState,
                userScrollEnabled = false
            ) { page ->
                NavigationPagerContent(
                    navController = navController,
                 onBackPressed = onBackPressed,
                    page = page
                )
            }
        }
    }
}

@Composable
fun NavigationPagerContent(
    navController: NavHostController,
        onBackPressed: () -> Unit,
    page: Int) {
    when (page) {
        0 -> {

            val context = LocalContext.current //
            BackHandler {
                (context as Activity).finish() //callback wenn zurückbutton gedrückt wird
            }

            val dashboardScreenViewModel: DashboardScreenViewModel = hiltViewModel()
            val dashboardScreenUIState by dashboardScreenViewModel.uiState.collectAsState()

            DashboardScreen(
                uiState = dashboardScreenUIState,
                onTodoCheckedChanged = dashboardScreenViewModel::checkTodo,
                onAddCategoryClick = {
                    navController.navigate(CategoryEntryDestination.route)
                },
                onCategoryClick = {
                    navController.navigate(CategoryDisplayDestination.destinationWithParam(it.id))
                },
                onTodoClick = {
                    navController.navigate(TodoDisplayDestination.destinationWithParam(it.id))
                }
            )
        }
//CalendarScreenViewModel
        //CalendarScreenUIState
        1 -> {
            val context = LocalContext.current
            BackHandler {
                (context as Activity).finish()
            }
            val calendarScreenViewModel: CalendarScreenViewModel = hiltViewModel()
            val calendarScreenUiState by calendarScreenViewModel.uiState.collectAsState()

            CalendarScreen(
                uiState = calendarScreenUiState,
                onTodoClick = {navController.navigate(TodoDisplayDestination.destinationWithParam(it.id)) },
                onTodoCheckedChanged = calendarScreenViewModel::checkTodo
            )
        }
//CategoriesScreenViewModel
        //CategoriesScreenUIState
        2 -> {
            val context = LocalContext.current
            BackHandler {
                (context as Activity).finish()
            }
            val categoriesScreenViewModel: CategoriesScreenViewModel = hiltViewModel()
            val categoriesScreenUiState by categoriesScreenViewModel.uiState.collectAsState()

            CategoriesScreen(
                uiState = categoriesScreenUiState,
                onAddCategoryClick = {
                    navController.navigate(CategoryEntryDestination.route)
                },
                onCategoryClick = {
                    navController.navigate(CategoryDisplayDestination.destinationWithParam(it.id))
                },
                onTodoClick = {
                    navController.navigate(TodoDisplayDestination.destinationWithParam(it.id))
                }

            )

        }
//SettingsScreenViewModel
        //SettingsScreenUIState
        3 -> {
            val context = LocalContext.current
            BackHandler {
                (context as Activity).finish()
            }

            val settingsScreenViewModel: SettingsScreenViewModel = hiltViewModel()
            val settingsScreenUiState by settingsScreenViewModel.uiState.collectAsState()

            SettingsScreen(
                uiState = settingsScreenUiState
                )
        }
    }
}

