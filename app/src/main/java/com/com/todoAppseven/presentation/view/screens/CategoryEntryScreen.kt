    package com.com.todoAppseven.presentation.view.screens

import android.widget.Toast
import androidx.activity.compose.BackHandler
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.twotone.ArrowBack
import androidx.compose.material.icons.twotone.Check
import androidx.compose.material.icons.twotone.Close
import androidx.compose.material.icons.twotone.Warning
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.CenterAlignedTopAppBar
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.OutlinedButton
import androidx.compose.material3.OutlinedTextField
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.LocalFocusManager
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextOverflow
import androidx.hilt.navigation.compose.hiltViewModel
import com.com.todoAppseven.R
import com.com.todoAppseven.presentation.helpers.NavDest
import com.com.todoAppseven.presentation.helpers.toComposeColor
import com.com.todoAppseven.presentation.view.components.ConfirmationDialog
import com.com.todoAppseven.presentation.viewmodel.CategoryEntryViewModel
import com.com.todoAppseven.presentation.viewmodel.exceptions.InvalidCatNameException
import com.com.todoAppseven.ui.theme.spacing
import com.vanpra.composematerialdialogs.MaterialDialog
import com.vanpra.composematerialdialogs.MaterialDialogState
import com.vanpra.composematerialdialogs.color.ColorPalette
import com.vanpra.composematerialdialogs.color.colorChooser
import com.vanpra.composematerialdialogs.rememberMaterialDialogState

object CategoryEntryDestination : NavDest(
    displayedTitle = R.string.category,
    route= "new_category"
){
    const val parameterName = "categoryId"
    private const val categoryRoute = "edit_category"

    val parametrizedRoute = "$categoryRoute/{$parameterName}"
    fun destinationWithParam(categoryId: Long) = "$categoryRoute/$categoryId"
}

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun CategoryEntryScreen(
    onNavigateToRoot: () -> Unit,
    onNavigateUp: () -> Unit,
    modifier: Modifier = Modifier
) {

    val viewModel = hiltViewModel<CategoryEntryViewModel>()
    val uiState by viewModel.uiState.collectAsState()
    val dialogState by viewModel.dialogState.collectAsState()
    val focusManager = LocalFocusManager.current

    val colorDialogState = rememberMaterialDialogState()

    ColorPickerDialog(colorDialogState) { viewModel.updateCategoryColor(it) }

    BackHandler { //zurücktaste
        if (uiState.newEntry) {
            onNavigateToRoot()
        } else viewModel.displayDialog()
    }

    Scaffold(
        modifier = modifier,
        topBar = {
            CenterAlignedTopAppBar(
                title = {
                    if (uiState.newEntry) Text(text = stringResource(id = R.string.neue_cat))
                    else Text(
                        uiState.categoryName ?: "",
                        maxLines = 1,
                        overflow = TextOverflow.Ellipsis
                    )
                },
                actions = {
                    val context = LocalContext.current
                    IconButton(
                        onClick = {
                            try {
                                viewModel.saveCategory()
                                onNavigateUp()
                            } catch (e: InvalidCatNameException) {
                                Toast.makeText(context, context.getText(e.displayMessage), Toast.LENGTH_SHORT).show()
                            }

                        }
                    ) {
                        Icon(imageVector = Icons.TwoTone.Check, contentDescription = null)
                    }
                },
                navigationIcon = {
                    IconButton(
                        onClick = {
                            if (uiState.newEntry) {
                                onNavigateToRoot()
                            } else viewModel.displayDialog()
                        }
                    ) {
                        Icon(
                            imageVector = if (uiState.newEntry) Icons.TwoTone.Close else Icons.TwoTone.ArrowBack,
                            contentDescription = null
                        )
                    }
                }
            )
        }
    ){
        CategoryEntryScreenContent(
            modifier = Modifier.padding(it),
            categoryName = uiState.categoryName ?: "",
            categoryColor = uiState.colorHolder.toComposeColor(),
            onColorClick = {
                focusManager.clearFocus()
                colorDialogState.show()
            },
            onCategoryNameChange = viewModel::updateCategoryName
        )
    }
    if(dialogState.shouldDisplayDialog) {
        ConfirmationDialog(
            onDismissRequest = viewModel::hideDialog,
            onConfirmation = {
                onNavigateUp()

            },
            dialogTitle = stringResource(R.string.bist_du_sicher),
            dialogText = stringResource(R.string.unsaved_category_changes_backhandler),
            icon = Icons.TwoTone.Warning
        )
    }
}

@Composable
private fun ColorPickerDialog(
    dialogState: MaterialDialogState,
    onColorSelected: (Color) -> Unit
) {
    MaterialDialog(
        dialogState = dialogState,
        backgroundColor = MaterialTheme.colorScheme.primaryContainer,
        shape = MaterialTheme.shapes.large,
        autoDismiss = true,
        elevation = MaterialTheme.spacing.default,
        buttons = {
            negativeButton(
                text = stringResource(id = R.string.abbrechen)
            )
            positiveButton(
                text = stringResource(id = R.string.bestaetigen)
            )
        }
    ) {
        colorChooser(
            colors = ColorPalette.Primary,
            //subColors = ColorPalette.PrimarySub,
            onColorSelected = onColorSelected
        )
    }
}

@Composable
private fun CategoryEntryScreenContent(
    categoryName: String,
    categoryColor: Color,
    onColorClick: () -> Unit,
    onCategoryNameChange: (String) -> Unit,
    modifier: Modifier = Modifier,
) {
    Surface(
        modifier = modifier
    ) {
        Column(
            modifier = Modifier.padding(horizontal = MaterialTheme.spacing.extraSmall),
            verticalArrangement = Arrangement.spacedBy(
                space = MaterialTheme.spacing.extraSmall,
                alignment = Alignment.Top
            )
        ) {
            OutlinedTextField(
                modifier = Modifier.fillMaxWidth(),
                maxLines = 1,
                label = {
                    Text(text = stringResource(R.string.category_name))
                },
                value = categoryName,
                onValueChange = onCategoryNameChange
            )
            OutlinedButton(
                onClick = onColorClick,
                colors = ButtonDefaults.outlinedButtonColors(contentColor = categoryColor),
                modifier = Modifier.fillMaxWidth()
            ) {
                Text(stringResource(R.string.pick_color))
            }
        }
    }
}