package com.com.todoAppseven.presentation.view.screens

import android.widget.Toast
import androidx.activity.compose.BackHandler
import androidx.compose.animation.AnimatedContent
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.imePadding
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.twotone.Add
import androidx.compose.material.icons.twotone.ArrowBack
import androidx.compose.material.icons.twotone.CalendarMonth
import androidx.compose.material.icons.twotone.Check
import androidx.compose.material.icons.twotone.Close
import androidx.compose.material.icons.twotone.Warning
import androidx.compose.material3.CenterAlignedTopAppBar
import androidx.compose.material3.DropdownMenuItem
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.ExposedDropdownMenuBox
import androidx.compose.material3.ExposedDropdownMenuDefaults
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.OutlinedTextField
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.material3.TextButton
import androidx.compose.material3.TopAppBarDefaults
import androidx.compose.material3.rememberDatePickerState
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.input.nestedscroll.nestedScroll
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextOverflow
import androidx.hilt.navigation.compose.hiltViewModel
import com.com.todoAppseven.R
import com.com.todoAppseven.R.*
import com.com.todoAppseven.data.model.Category
import com.com.todoAppseven.data.model.Step
import com.com.todoAppseven.presentation.helpers.NavDest
import com.com.todoAppseven.presentation.view.components.ConfirmationDialog
import com.com.todoAppseven.presentation.view.components.InputField
import com.com.todoAppseven.presentation.view.components.StepTile
import com.com.todoAppseven.presentation.view.components.DatePicker
import com.com.todoAppseven.presentation.viewmodel.TodoEntryUiState
import com.com.todoAppseven.presentation.viewmodel.TodoEntryViewModel
import com.com.todoAppseven.presentation.viewmodel.exceptions.InvalidTodoNameException
import com.com.todoAppseven.ui.theme.spacing
import java.time.ZoneOffset
import java.time.format.DateTimeFormatter


object TodoEntryDestination : NavDest (
    displayedTitle = string.new_todo,
    route = "new_todo"
) {
    const val parameterName = "todoId"
    private const val editTodoRoute = "edit_todo"

    const val parametrizedRoute = "$editTodoRoute/{$parameterName}"
    fun destinationWithParam(todoId: Long) = "$editTodoRoute/$todoId"
}
@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun TodoEntryScreen(
    onNavigateToRoot: () -> Unit,
    onNavigateUp: () -> Unit,
    modifier: Modifier = Modifier
) {
    val viewModel = hiltViewModel<TodoEntryViewModel>()
    val uiState by viewModel.uiState.collectAsState()
    val dialogState by viewModel.dialogState.collectAsState()
    val datePickerDialogState by viewModel.datePickerDialogState.collectAsState()
    val scrollBehavior = TopAppBarDefaults.enterAlwaysScrollBehavior()
    BackHandler {
        if(uiState.newEntry) {
            onNavigateToRoot()
        } else viewModel.displayDialog()
    }
    //val scrollState = rememberScrollState()
    Scaffold(
        modifier = modifier.fillMaxWidth()
        .nestedScroll(scrollBehavior.nestedScrollConnection),
    //.verticalScroll(scrollState),
        topBar = {
            CenterAlignedTopAppBar(
                title = {
                    Text(
                        text = if(uiState.newEntry) stringResource(id = string.new_todo) else uiState.todoName,
                        maxLines = 1,
                        overflow = TextOverflow.Ellipsis
                    )
                },
                actions = {
                    val context = LocalContext.current
                    IconButton(
                        onClick = {
                            try {
                                if(uiState.newEntry) viewModel.addNewTodoWithSteps() else viewModel.updateTodoWithSteps()
                                if(uiState.newEntry) {
                                    onNavigateToRoot()
                                }
                                else  onNavigateUp()
                            } catch (e: InvalidTodoNameException) {
                                Toast.makeText(context, context.getText(e.displayMessage), Toast.LENGTH_SHORT).show()
                            }
                        }) {
                        Icon(imageVector = Icons.TwoTone.Check, contentDescription = null)
                    }
                },
                navigationIcon = {
                    IconButton(
                        onClick = {
                            if(uiState.newEntry) {
                                onNavigateToRoot()
                            } else viewModel.displayDialog()
                        }
                    ) {
                        Icon(
                            imageVector = if(uiState.newEntry) Icons.TwoTone.Close else Icons.TwoTone.ArrowBack,
                            contentDescription = null
                        )
                    }
                },
                scrollBehavior = scrollBehavior
            )
        }
    ) {
           val context = LocalContext.current

        TodoEntryScreenContent(
            uiState = uiState,
            onTodoNameValueChanged = viewModel::updateTodoName,
            onTodoDescriptionValueChanged = viewModel::updateTodoDescription,
            onCategorySelected = viewModel::updateSelectedCategory,
            onDateValueChanged = {  },
            onDatePickerClick = viewModel::displayDatePickerDialog,
            modifier = Modifier.padding(it)
                .imePadding(),
            onStepChecked = { _, _ -> },
            onStepRemoveClick = viewModel::deleteStep,
            onStepDescriptionValueChanged = viewModel::updateStepDescription,
            onStepConfirmClick = {
                    viewModel.addStep(it)
                    viewModel.switchStepEntryMode(false)
            },
            onStepCancelClick = { viewModel.switchStepEntryMode(false) },
            onAddStepClick = { viewModel.switchStepEntryMode(true) },
        )
    }
    if(dialogState.shouldDisplayDialog) {
        ConfirmationDialog(
            onDismissRequest = viewModel::hideDialog,
            onConfirmation = {
                onNavigateUp()
            },
            dialogTitle = stringResource(string.bist_du_sicher),
            dialogText = stringResource(string.unsaved_todo_changes),
            icon = Icons.TwoTone.Warning
        )
    }

    if(datePickerDialogState.shouldDisplayDialog) {
        val pickerState = rememberDatePickerState(
            initialSelectedDateMillis = uiState.dueDate.atStartOfDay().toEpochSecond(ZoneOffset.UTC) * 1000L, //we need millis
            initialDisplayedMonthMillis = uiState.dueDate.atStartOfDay().toEpochSecond(ZoneOffset.UTC) * 1000L //same here
        )
        DatePicker(
            onEntryValueChanged = viewModel::updateDueDate,
            onDismissRequest = viewModel::hideDatePickerDialog,
            title = "",
            headline = stringResource(id = string.due_date),
            confirmEnabled = (pickerState.selectedDateMillis != null),
            confirmButtonText = stringResource(id = string.bestaetigen),
            dismissButtonText = stringResource(id = string.abbrechen),
            datePickerState = pickerState
        )
    }
}

@Composable
fun TodoEntryScreenContent(
    uiState: TodoEntryUiState,
    onTodoNameValueChanged: (String)->Unit,
    onTodoDescriptionValueChanged: (String) -> Unit,
    onCategorySelected: (Category) -> Unit,
    onDateValueChanged: (String) -> Unit,
    onDatePickerClick: () -> Unit,
    modifier: Modifier = Modifier,
    onStepChecked: (Step, Boolean) -> Unit,
    onStepRemoveClick: (Step) -> Unit,
    onStepDescriptionValueChanged: (String) -> Unit,
    onStepConfirmClick: (String) -> Unit,
    onStepCancelClick: () -> Unit,
    onAddStepClick: () -> Unit,
) {
    Surface(modifier = modifier.fillMaxSize()) {
        Column(
            modifier = Modifier.padding(horizontal = MaterialTheme.spacing.small),
            verticalArrangement = Arrangement.spacedBy(MaterialTheme.spacing.small)
        ) {
            InputField(
                modifier = Modifier.fillMaxWidth(),
                label = stringResource(string.todo_name),
                value = uiState.todoName,
                onValueChanged = onTodoNameValueChanged,
                maxLines = 1,
                singleLine = true
            )

            InputField(
                modifier = Modifier.fillMaxWidth(),
                label = stringResource(string.todo_description),
                value = uiState.todoDescription ?: "",
                onValueChanged = onTodoDescriptionValueChanged,
                maxLines = 5,
                singleLine = false
            )

            var menuExpanded by remember {
                mutableStateOf(false)
            }

            CategoryPickerDropdownMenu(
                modifier = Modifier.fillMaxWidth(),
                expanded = menuExpanded,
                label = stringResource(id = string.category),
                uiState = uiState,
                onExpandedChanged = {
                    menuExpanded = !menuExpanded
                },
                onCategorySelected = {
                    onCategorySelected(it)
                    menuExpanded = !menuExpanded
                }
            )

            InputField(
                modifier = Modifier.fillMaxWidth(),
                label = stringResource(string.due_date),
                value = uiState.dueDate.format(DateTimeFormatter.ofPattern("dd MMM yyyy")),
                onValueChanged = onDateValueChanged,
                trailingIcon = {
                    IconButton(onClick = onDatePickerClick) {
                        Icon(imageVector = Icons.TwoTone.CalendarMonth, contentDescription = null)
                    }
                },
                readOnly = true
            )
            Text(text = stringResource(string.steps), style = MaterialTheme.typography.labelMedium)
            LazyColumn(
                verticalArrangement = Arrangement.spacedBy(MaterialTheme.spacing.small),
                modifier = Modifier.imePadding()
            ) {
                items(uiState.stepsList) {
                    StepTile(
                        step = it,
                        onStepChecked = onStepChecked,
                        onRemoveClick = onStepRemoveClick,
                        editMode = true
                    )
                }
                item {
                    AnimatedContent(targetState = uiState.isAddingNewStep, label = "Animated input tile") {
                        if(it) {
                            StepEntryTile(
                                stepDescription = uiState.stepDescription ?: "",
                                onDescriptionValueChanged = onStepDescriptionValueChanged,
                                onConfirmClick = onStepConfirmClick,
                                onCancelClick = onStepCancelClick
                            )
                        } else {
                            NewStepTile(
                                text = stringResource(string.add_step),
                                icon = Icons.TwoTone.Add,
                                onClick = onAddStepClick
                            )
                        }
                    }
                }
            }
        }
    }
}
@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun CategoryPickerDropdownMenu(
    uiState: TodoEntryUiState,
    label: String,
    modifier: Modifier = Modifier,
    onExpandedChanged: (Boolean) -> Unit,
    onCategorySelected: (Category) -> Unit,
    expanded: Boolean = false
) {
    ExposedDropdownMenuBox(
        modifier = modifier,
        expanded = expanded,
        onExpandedChange = onExpandedChanged
    ) {
        InputField(
            modifier = Modifier
                .menuAnchor()
                .fillMaxWidth(),
            label = label,
            value = uiState.selectedCategoryName ?: "",
            onValueChanged = { },
            maxLines = 1,
            readOnly = true,
            singleLine = true,
            trailingIcon = { ExposedDropdownMenuDefaults.TrailingIcon(expanded = expanded) }
        )
        ExposedDropdownMenu(
            modifier = Modifier.fillMaxWidth(),
            expanded = expanded,
            onDismissRequest = {
                onExpandedChanged(false)
            }
        ) {
            if(uiState.categoryList.isEmpty()) {
                DropdownMenuItem(
                    text = {
                        Text(text = stringResource(string.no_categories))
                    },
                    onClick = { onExpandedChanged(false) }
                )
            } else {
                uiState.categoryList.forEach {
                    DropdownMenuItem(
                        text = { Text(it.name) },
                        onClick = {
                            onCategorySelected(it)
                        }
                    )
                }
            }
        }
    }
}

@Composable
fun StepEntryTile(
    stepDescription: String,
    onDescriptionValueChanged: (String) -> Unit,
    onConfirmClick: (String) -> Unit,
    onCancelClick: () -> Unit,
    modifier: Modifier = Modifier
) {
//    val scrollState = rememberScrollState()
    Surface(
        modifier = modifier
            .fillMaxWidth(),
//            .verticalScroll(scrollState),
        shape = MaterialTheme.shapes.medium
    ) {
        Row(
            verticalAlignment = Alignment.CenterVertically
        ) {
            OutlinedTextField(
                modifier = Modifier.fillMaxWidth(),
                label = {
                    Text(text = stringResource(string.step_description))
                },
                shape = MaterialTheme.shapes.medium,
                value = stepDescription,
                onValueChange = onDescriptionValueChanged,
                maxLines = 1,
                trailingIcon = {
                    Row {
                        IconButton(onClick = { onConfirmClick(stepDescription) }){
                            Icon(imageVector = Icons.TwoTone.Check, contentDescription = null)
                        }
                        IconButton(onClick = onCancelClick) {
                            Icon(imageVector = Icons.TwoTone.Close, contentDescription = null)
                        }
                    }
                }
            )
        }
    }
}

@Composable
fun NewStepTile(
    text: String,
    icon: ImageVector,
    onClick: () -> Unit,
    modifier: Modifier = Modifier
) {
    Surface(
        modifier = modifier.fillMaxWidth(),
        shape = MaterialTheme.shapes.medium
    ){
        TextButton(
            onClick = onClick,
            contentPadding = PaddingValues(MaterialTheme.spacing.medium),
            shape = MaterialTheme.shapes.medium
        ) {
            Icon(imageVector = icon, contentDescription = null)
            Text(text = text)
        }
    }

}
