package com.com.todoAppseven.presentation.view.screens.pagerNav

import android.annotation.SuppressLint
import androidx.compose.animation.AnimatedVisibility
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.RowScope
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.twotone.Check
import androidx.compose.material.icons.twotone.Settings
import androidx.compose.material3.Button
import androidx.compose.material3.Card
import androidx.compose.material3.CenterAlignedTopAppBar
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.OutlinedTextField
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.material3.TopAppBar
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import com.com.todoAppseven.R
import com.com.todoAppseven.presentation.helpers.NavDest
import com.com.todoAppseven.presentation.view.components.InputField
import com.com.todoAppseven.presentation.viewmodel.SettingsScreenUiState
import com.com.todoAppseven.presentation.viewmodel.SettingsScreenViewModel
import com.com.todoAppseven.ui.theme.spacing
import dagger.Lazy

object SettingsScreenDestination : NavDest(
    displayedTitle = R.string.settings,
    route = "settings",
    Icons.TwoTone.Settings
)

@Composable
@SuppressLint("UnusedMaterial3ScaffoldPaddingParameter")
fun SettingsScreen(
    uiState: SettingsScreenUiState,
    modifier: Modifier = Modifier,
   // onSaveUsernameClick: () -> Unit,
) {
    val viewModel = hiltViewModel<SettingsScreenViewModel>()
    val uiState by viewModel.uiState.collectAsState()

    Surface(modifier = modifier.fillMaxSize()) {
        Column(
            modifier = Modifier.padding(horizontal = MaterialTheme.spacing.small),
            verticalArrangement = Arrangement.spacedBy(MaterialTheme.spacing.small)
        ) {
            InputField(modifier = Modifier.fillMaxWidth(),
                label = stringResource(R.string.username),
                value = uiState.username,
                onValueChanged = { newUsername -> viewModel.updateUsername(newUsername)},
                maxLines = 1,
                singleLine = true,
                trailingIcon = {
                    IconButton(onClick = { viewModel.updateUsername("") }) {
                        Icon(imageVector = Icons.TwoTone.Check, contentDescription = null)
                    }
                }
            )
        }
    }
}


//    OutlinedTextField(value = uiState.username, onValueChange = { newUsername ->
//        viewModel.updateUsername(newUsername)
//    } )
