package com.com.todoAppseven.presentation.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.com.todoAppseven.data.di.annotation.RoomDataSource
import com.com.todoAppseven.data.model.Todo
import com.com.todoAppseven.data.repository.CategoryRepo
import com.com.todoAppseven.data.repository.TodoRepo
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.flow.filter
import kotlinx.coroutines.flow.stateIn
import kotlinx.coroutines.launch
import java.time.LocalDate
import java.time.LocalDateTime
import javax.inject.Inject

data class CalendarScreenUiState(
    val todaysTodos: List<Todo>,
    val sortedTodosList: List<Todo>,
   // val dueDate: LocalDate

)

@HiltViewModel
class CalendarScreenViewModel @Inject constructor(
    @RoomDataSource private val todoRepo: TodoRepo,
    @RoomDataSource private val categoryRepo: CategoryRepo
) : ViewModel() {

    val uiState = todoRepo.getAllTodos()
        .combine(todoRepo.getAllUncategorizedTodos()) {
            todos, sortedTodosList ->
            val todaysTodos = todos.filter {
                it.dueDate == LocalDate.now()
            }
            val sortedTodosList = todos.filter {
                it.dueDate != LocalDate.now()
            }
            CalendarScreenUiState(todaysTodos,sortedTodosList)
        }.stateIn(
            viewModelScope,
             SharingStarted.WhileSubscribed(TIMEOUT_MILLIS),
            CalendarScreenUiState(listOf(), listOf())
        )


    fun checkTodo(todo: Todo, checked: Boolean) {
        viewModelScope.launch {
            if (todo.isDone) {
                //todo unchecked
                if (!checked) {
                    todoRepo.updateTodo(todo.copy(isDone = false, completedOn = null))
                }
            } else { //todo checked
                if (checked) {
                    todoRepo.updateTodo(todo.copy(isDone = true, completedOn = LocalDateTime.now()))
                }
            }
        }
    }

    companion object {
        val TIMEOUT_MILLIS = 5000L
    }


}






















