package com.com.todoAppseven.presentation.view.screens

import androidx.activity.compose.BackHandler
import androidx.compose.animation.AnimatedContent
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.twotone.ArrowBack
import androidx.compose.material.icons.twotone.Check
import androidx.compose.material.icons.twotone.Close
import androidx.compose.material.icons.twotone.Delete
import androidx.compose.material.icons.twotone.Edit
import androidx.compose.material.icons.twotone.Warning
import androidx.compose.material3.Card
import androidx.compose.material3.CenterAlignedTopAppBar
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.material3.TextButton
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextOverflow
import androidx.hilt.navigation.compose.hiltViewModel
import com.com.todoAppseven.R
import com.com.todoAppseven.data.model.Step
import com.com.todoAppseven.presentation.helpers.NavDest
import com.com.todoAppseven.presentation.helpers.NavigationType
import com.com.todoAppseven.presentation.view.components.ConfirmationDialog
import com.com.todoAppseven.presentation.view.components.StepTile
import com.com.todoAppseven.presentation.viewmodel.TodoDisplayUiState
import com.com.todoAppseven.presentation.viewmodel.TodoDisplayViewModel
import com.com.todoAppseven.ui.theme.spacing
import java.time.format.DateTimeFormatter

object TodoDisplayDestination: NavDest(
    displayedTitle = R.string.todo,
    route = "todo"
){
    override val route: String = super.route
    const val parameterName = "stepId"

    val parametrizedRoute = "$route/{$parameterName}"
    fun destinationWithParam(todoId: Long) = "$route/$todoId"

}
@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun TodoDisplayScreen(
    onNavigateUp: () -> Unit,
    onNavigateToRoot: () -> Unit,
    onEditClick: (Long) -> Unit,
    modifier: Modifier = Modifier
){
    val viewModel = hiltViewModel<TodoDisplayViewModel>()
    val uiState by viewModel.uiState.collectAsState()
    val dialogState by viewModel.dialogState.collectAsState()

    BackHandler {
        onNavigateUp
    }

    Scaffold(
        modifier = modifier,
        topBar = {
            CenterAlignedTopAppBar(
                title = { Text(uiState.todo.name, maxLines = 1, overflow = TextOverflow.Ellipsis) },
                actions = {
                    //Edit
                    if (uiState.todo.id != 0L) {
                        IconButton(
                            onClick = {
                                onEditClick(uiState.todo.id)
                            }
                        ) {
                            Icon(imageVector = Icons.TwoTone.Edit, contentDescription = null)
                        }
                        //Delete
                        IconButton(onClick = viewModel::displayDialog) {
                            Icon(imageVector = Icons.TwoTone.Delete, contentDescription = null)
                        }
                    }
                },
                navigationIcon = {
                    IconButton(
                        onClick = { onNavigateUp() }
                    ) {
                        Icon(
                            imageVector = Icons.TwoTone.ArrowBack ,
                            contentDescription = null
                        )
                    }
                })
        }
    ){
        TodoDisplayScreenContent(
            uiState = uiState,
            onStepChecked = viewModel::checkStep,
            onTodoChecked = viewModel::checkTodo,
            modifier = Modifier.padding(it),
        )
        if (dialogState.shouldDisplayDialog) {
            ConfirmationDialog(
                onDismissRequest = viewModel::hideDialog,
                onConfirmation = {
                    viewModel.deleteTodo()
                    viewModel.hideDialog()
                    onNavigateToRoot()
                },
                dialogTitle = stringResource(id = R.string.loeschen_best_titel),
                dialogText = stringResource(R.string.confirm_deletion_todo),
                icon = Icons.TwoTone.Warning
            )
        }
    }
}
@Composable
private fun TodoDisplayScreenContent(
    uiState: TodoDisplayUiState,
    onStepChecked: (Step, Boolean) -> Unit,
onTodoChecked: (Boolean)->Unit,
    modifier: Modifier = Modifier
){
    Surface(
        modifier = modifier.fillMaxSize()
    ) {
        Column(
            modifier = Modifier.padding(MaterialTheme.spacing.small),
            verticalArrangement = Arrangement.spacedBy(MaterialTheme.spacing.small)
        ) {
            var descriptionExpanded by rememberSaveable {
                mutableStateOf(false)
            }
            Text(
                text = "${stringResource(id = R.string.todo_description)}:"
            )
            if (uiState.todo.description != null) {
                Card(
                    modifier = Modifier.fillMaxWidth(),
                    onClick = {
                        descriptionExpanded = !descriptionExpanded
                    },
                    shape = MaterialTheme.shapes.medium
                ) {
                    AnimatedContent(
                        targetState = descriptionExpanded,
                        label = "Todo description animation"
                    ) {
                        Text(
                            modifier = Modifier.padding(MaterialTheme.spacing.medium),
                            text = uiState.todo.description ?: "",
                            maxLines = if (it) Int.MAX_VALUE else 1,
                            overflow = TextOverflow.Ellipsis
                        )
                    }
                }
            }
            val dateFormat = "EEEE, dd MMMM yyyy"

            TodoFieldDisplay(
                fieldName = stringResource(id = R.string.due_date),
                fieldValue = uiState.todo.dueDate.format(DateTimeFormatter.ofPattern(dateFormat))
            )

            TodoFieldDisplay(
                fieldName = stringResource(R.string.erstellt_am),
                fieldValue = uiState.todo.createdOn.toLocalDate()
                    .format(DateTimeFormatter.ofPattern(dateFormat))
            )
            TodoFieldDisplay(
                fieldName = stringResource(R.string.abgeschlossen_am),
                fieldValue = uiState.todo.completedOn?.toLocalDate()
                    ?.format(DateTimeFormatter.ofPattern(dateFormat)) ?: ""
            )
            TodoFieldDisplay(
                fieldName = stringResource(id = R.string.category),
                fieldValue = uiState.category.name
            )

            TextButton(
                modifier = Modifier.fillMaxWidth(),
                onClick = {
                    onTodoChecked(!uiState.todo.isDone)
                },
                contentPadding = PaddingValues(MaterialTheme.spacing.small),
                shape = MaterialTheme.shapes.medium
            ) {
                Icon(
                    imageVector = if (uiState.todo.isDone) Icons.TwoTone.Close else Icons.TwoTone.Check,
                    contentDescription = null
                )
                Text(
                    text = (if (uiState.todo.isDone) "Als nicht erledigt markieren " else "Als erledigt markieren") //.uppercase()
                )
            }

            LazyColumn(
                verticalArrangement = Arrangement.spacedBy(MaterialTheme.spacing.small)
            ) {
                items(uiState.steps) { step ->
                    StepTile(
                        step = step,
                        onStepChecked = onStepChecked,
                        onRemoveClick = {/*nix */ },
                        editMode = false
                    )
                }
            }
        }
    }
}
@Composable
private fun TodoFieldDisplay(
    fieldName: String,
    fieldValue: String,
    modifier: Modifier = Modifier
) {
    Row(
        modifier = modifier,
        verticalAlignment = Alignment.CenterVertically,
        horizontalArrangement = Arrangement.SpaceBetween
    ) {
        Text(text = "$fieldName:")
        Spacer(modifier = Modifier.weight(1F))
        Text(text = fieldValue)
    }
}