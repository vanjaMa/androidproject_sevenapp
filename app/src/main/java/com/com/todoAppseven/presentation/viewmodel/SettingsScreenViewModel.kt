package com.com.todoAppseven.presentation.viewmodel

import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.preferencesdatastore.di.annotation.DataStorePreferences
import com.example.preferencesdatastore.repository.PrefRepo
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.stateIn
import kotlinx.coroutines.launch
import javax.inject.Inject

data class SettingsScreenUiState(
    val username: String
)

@HiltViewModel
class SettingsScreenViewModel @Inject constructor(
@DataStorePreferences private val prefRepo: PrefRepo,
    savedStateHandle: SavedStateHandle
): ViewModel() {


val uiState = prefRepo.getUsername().map {
    if (it.isNullOrBlank()){
       SettingsScreenUiState("")
    }else { SettingsScreenUiState(it)}
}.stateIn(
    viewModelScope, SharingStarted.WhileSubscribed(TIMEOUT_MILLIS),
    SettingsScreenUiState("")
)

    fun updateUsername(newUsername: String){
        viewModelScope.launch {
            prefRepo.setUsername(newUsername)
           // prefRepo.setIsLeavingSettings(true)
        }

    }
    companion object {
        val TIMEOUT_MILLIS = 5000L
    }
}
