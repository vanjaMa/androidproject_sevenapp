package com.com.todoAppseven.presentation.viewmodel

import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.viewModelScope
import com.com.todoAppseven.data.di.annotation.RoomDataSource
import com.com.todoAppseven.data.model.Category
import com.com.todoAppseven.data.model.Step
import com.com.todoAppseven.data.model.Todo
import com.com.todoAppseven.data.repository.CategoryRepo
import com.com.todoAppseven.data.repository.StepRepo
import com.com.todoAppseven.data.repository.TodoRepo
import com.com.todoAppseven.presentation.view.screens.TodoDisplayDestination
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.flow.filterNotNull
import kotlinx.coroutines.flow.stateIn
import kotlinx.coroutines.launch
import java.time.LocalDate
import java.time.LocalDateTime
import javax.inject.Inject

data class TodoDisplayUiState(
    val todo: Todo,
    val category: Category,
    val steps: List<Step>
)
@HiltViewModel
class TodoDisplayViewModel @Inject constructor(
    savedStateHandle: SavedStateHandle, //wie eine magische schublade ->speichert infos die man später abrufen möchte
    @RoomDataSource private val todoRepo: TodoRepo,
    @RoomDataSource private val categoryRepo: CategoryRepo,
    @RoomDataSource private val stepRepo: StepRepo,
  //  @RoomDataSource private val userRepo: UserRepo
): DialogViewModel(){
    private val todoId = savedStateHandle[TodoDisplayDestination.parameterName] ?: 0L //standardwert wenn asu saveStateHandle null gelesen wird-->Long Zahl

val uiState = todoRepo.getTodoWithStepsById(todoId).filterNotNull().combine(categoryRepo.getAllCategories()) {todo, categories ->
    TodoDisplayUiState(
        todo = todo.todo,
        category = categories.firstOrNull() {it.id ==todo.todo.categoryId} ?: Category.uncategorizedCategory(),
        steps = todo.steps
    )
}.stateIn(
    viewModelScope,
  SharingStarted.WhileSubscribed(TIMEOUT_MILLIS),

    TodoDisplayUiState(
        todo = Todo(
            name = "Todo nicht gefunden",
            description = "Todo nicht gefunden",
            createdOn = LocalDateTime.now(),
            dueDate = LocalDate.now(),
            isDone = false
        ),
        category = Category.uncategorizedCategory(),
        steps = listOf()
    )
)

    fun deleteTodo() {
        viewModelScope.launch {
            todoRepo.removeTodo(uiState.value.todo)
        }
    }

    fun checkTodo(checked: Boolean) {
        viewModelScope.launch {
            //todo unchecked
            if (uiState.value.todo.isDone && !checked) {
                todoRepo.updateTodo(uiState.value.todo.copy(isDone = false, completedOn = null))
            } else {
                //todo checked
                if (checked) {
                    todoRepo.removeTodo(
                        uiState.value.todo.copy(
                            isDone = true,
                            completedOn = LocalDateTime.now()
                        )
                    )
                }
            }
        }
    }

    fun checkStep(step: Step, checked: Boolean) {
        viewModelScope.launch {
            if(step.isDone && !checked) {
                stepRepo.updateStep(step.copy(isDone = false))
            } else {
                if(checked) {
                    stepRepo.updateStep(step.copy(isDone = true))
                }
            }
        }
    }
    companion object {
        val TIMEOUT_MILLIS = 5000L
    }

}





