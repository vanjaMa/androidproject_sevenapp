package com.com.todoAppseven.presentation.view.navigation

import androidx.compose.runtime.Composable
import androidx.navigation.NavGraphBuilder
import androidx.navigation.NavType
import androidx.navigation.compose.composable
import androidx.navigation.navArgument
import com.com.todoAppseven.presentation.EntryDestination
import com.com.todoAppseven.presentation.view.screens.CategoryDisplayDestination
import com.com.todoAppseven.presentation.view.screens.CategoryEntryDestination
import com.com.todoAppseven.presentation.view.screens.TodoDisplayDestination
import com.com.todoAppseven.presentation.view.screens.TodoEntryDestination

fun NavGraphBuilder.sevenAppNavGraph(
    entryContent: @Composable (() -> Unit),
    categoryEntryContent: @Composable (() -> Unit),
    displayCategoryContent: @Composable (() -> Unit),
    todoEntryContent: @Composable (() -> Unit),
    displayTodoContent: @Composable (() -> Unit)

) {
    composable(route = EntryDestination.route) {
        entryContent()
    }
    composable(route = CategoryEntryDestination.route) {
        categoryEntryContent()
    }
    composable(
        route = CategoryEntryDestination.parametrizedRoute,
        arguments = listOf(navArgument(CategoryEntryDestination.parameterName) { type = NavType.LongType })
    ) {
        categoryEntryContent()
    }

    composable(
        route = CategoryDisplayDestination.parametrizedRoute,
        arguments = listOf(navArgument(CategoryDisplayDestination.parameterName) { type = NavType.LongType })
    ) {
        displayCategoryContent()
    }

    composable(route = TodoEntryDestination.route) {
        todoEntryContent()
    }

    composable(
        route = TodoEntryDestination.parametrizedRoute,
        arguments = listOf(navArgument(TodoEntryDestination.parameterName) { type = NavType.LongType })
    ) {
        todoEntryContent()
    }

    composable(
        route = TodoDisplayDestination.parametrizedRoute,
        arguments = listOf(navArgument(TodoDisplayDestination.parameterName) { type = NavType.LongType })
    ) {
        displayTodoContent()
    }

}