package com.com.todoAppseven.presentation.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.preferencesdatastore.di.annotation.DataStorePreferences
import com.example.preferencesdatastore.repository.PrefRepo
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.stateIn
import javax.inject.Inject

data class SevenAppState(
    val username: String,
    val shouldDisplayWelcomeScreen: Boolean
)
@HiltViewModel
class SevenAppViewModel @Inject constructor(
    @DataStorePreferences private val prefRepo: PrefRepo
): ViewModel() {

    val state = prefRepo.getUsername().map{
        if(it.isNullOrBlank()){
            SevenAppState("User",true)
        }else{
            SevenAppState(it,false)
        }
    }.stateIn(
        viewModelScope,
         SharingStarted.WhileSubscribed(TIMEOUT_MILLIS),
        SevenAppState("User",false)
    )

    companion object {
        val TIMEOUT_MILLIS = 5000L
    }
}