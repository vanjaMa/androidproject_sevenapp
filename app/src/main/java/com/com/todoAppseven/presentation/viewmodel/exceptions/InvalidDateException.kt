package com.com.todoAppseven.presentation.viewmodel.exceptions

import com.com.todoAppseven.R

class InvalidDateException( override val displayMessage: Int = R.string.past_date
) : SevenAppException(message = "Datum ist in der Vergangenheit!")