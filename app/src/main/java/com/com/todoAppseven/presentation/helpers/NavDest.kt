package com.com.todoAppseven.presentation.helpers

import androidx.annotation.StringRes
import androidx.compose.ui.graphics.vector.ImageVector
import com.com.todoAppseven.presentation.view.screens.pagerNav.CalendarDestination
import com.com.todoAppseven.presentation.view.screens.pagerNav.CategoriesScreenDestination
import com.com.todoAppseven.presentation.view.screens.pagerNav.DashboardDestination
import com.com.todoAppseven.presentation.view.screens.pagerNav.SettingsScreenDestination

open class NavDest(
// Ressourcen-ID für Titel
    @StringRes  val displayedTitle: Int,
    // Routenpfad, überschreibbar
   open  val route: String,
    // Optionales Navigationssymbol, Standardwert null
    val navigationIcon: ImageVector? = null
)

fun getPagerDestinationsList() = listOf(
    DashboardDestination,
   CalendarDestination,
    CategoriesScreenDestination,
    SettingsScreenDestination,
)
