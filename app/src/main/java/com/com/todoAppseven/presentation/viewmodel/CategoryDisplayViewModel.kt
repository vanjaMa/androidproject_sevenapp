package com.com.todoAppseven.presentation.viewmodel

import androidx.compose.ui.graphics.Color
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.viewModelScope
import com.com.todoAppseven.data.di.annotation.RoomDataSource
import com.com.todoAppseven.data.model.Category
import com.com.todoAppseven.data.model.Todo
import com.com.todoAppseven.data.repository.CategoryRepo
import com.com.todoAppseven.data.repository.TodoRepo
import com.com.todoAppseven.presentation.helpers.toColorHolder
import com.com.todoAppseven.presentation.view.screens.CategoryDisplayDestination
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.filterNotNull
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.stateIn
import kotlinx.coroutines.launch
import java.time.LocalDateTime
import javax.inject.Inject


data class CategoryDisplayUiState(
    val category: Category,
    val todoList: List<Todo>
)
@HiltViewModel
class CategoryDisplayViewModel @Inject constructor(
    savedStateHandle: SavedStateHandle,
    @RoomDataSource private val categoryRepo: CategoryRepo,
    @RoomDataSource private val todoRepo: TodoRepo
) : DialogViewModel(){

    private val categoryId: Long = savedStateHandle[CategoryDisplayDestination.parameterName] ?: 0

    val uiState = (
            if (categoryId == 0L)
                todoRepo.getAllUncategorizedTodos().map {
                    CategoryDisplayUiState(
                        category = Category.uncategorizedCategory(),
                        todoList = it
                    )
                } else
                categoryRepo.getCategoryWithTodoById(categoryId).filterNotNull().map {
                    CategoryDisplayUiState(
                        category = it.category,
                        todoList = it.todos
                    )
                }).stateIn(
        viewModelScope,
   SharingStarted.WhileSubscribed(TIMEOUT_MILLIS),
        CategoryDisplayUiState(
            Category(name = "",
                color = Color.Gray.toColorHolder()),
            listOf()
        )
    )

    fun deleteCategory() {
        viewModelScope.launch {
            categoryRepo.removeCategory(uiState.value.category)
        }
    }
    fun checkTodo(todo: Todo, checked: Boolean) {
        viewModelScope.launch {
            if (todo.isDone) {
                //todo unchecked
                if (!checked) {
                    todoRepo.updateTodo(todo.copy(isDone = false, completedOn = null))
                }
            } else {
                //todo checked
                if (checked) {
                    todoRepo.updateTodo(
                        todo.copy(
                            isDone = true,
                            completedOn = LocalDateTime.now()
                        )
                    )
                }
            }
        }
    }
    companion object {
        val TIMEOUT_MILLIS = 5000L
    }
}


