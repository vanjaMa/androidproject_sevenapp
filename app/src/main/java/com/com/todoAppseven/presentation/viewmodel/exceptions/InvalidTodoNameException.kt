package com.com.todoAppseven.presentation.viewmodel.exceptions

import com.com.todoAppseven.R

class InvalidTodoNameException(
    override val displayMessage: Int = R.string.todo_name_warning
    ) : SevenAppException("Todoname darf nicht fehlen!")
