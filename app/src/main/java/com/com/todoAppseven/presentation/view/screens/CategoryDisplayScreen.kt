package com.com.todoAppseven.presentation.view.screens

import androidx.activity.compose.BackHandler
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.twotone.ArrowBack
import androidx.compose.material.icons.twotone.Delete
import androidx.compose.material.icons.twotone.Edit
import androidx.compose.material.icons.twotone.TaskAlt
import androidx.compose.material.icons.twotone.Warning
import androidx.compose.material3.CenterAlignedTopAppBar
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextOverflow
import androidx.hilt.navigation.compose.hiltViewModel
import com.com.todoAppseven.R
import com.com.todoAppseven.data.model.Todo
import com.com.todoAppseven.presentation.helpers.NavDest
import com.com.todoAppseven.presentation.helpers.NavigationType
import com.com.todoAppseven.presentation.view.components.ConfirmationDialog
import com.com.todoAppseven.presentation.view.components.TodoTile
import com.com.todoAppseven.presentation.viewmodel.CategoryDisplayUiState
import com.com.todoAppseven.presentation.viewmodel.CategoryDisplayViewModel
import com.com.todoAppseven.ui.theme.spacing


object CategoryDisplayDestination : NavDest(
    displayedTitle = R.string.category,
    route = "category"
) {
    override val route: String = super.route
    const val parameterName = "categoryId"

    val parametrizedRoute = "$route/{$parameterName}"
    fun destinationWithParam(categoryId: Long) = "$route/$categoryId"
}

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun CategoryDisplayScreen(
    onEditClick: (Long) -> Unit,
    onNavigateToRoot: () -> Unit,
    onNavigateUp: () -> Unit,
    onTodoClick: (Long) -> Unit,
    modifier: Modifier = Modifier
){
    val viewModel = hiltViewModel<CategoryDisplayViewModel>()
    val uiState by viewModel.uiState.collectAsState()
    val dialogState by viewModel.dialogState.collectAsState()

    BackHandler {
        onNavigateUp()
    }

    Scaffold(
        modifier = modifier,
        topBar = {
            CenterAlignedTopAppBar(
                title = { Text(uiState.category.name,
                    maxLines = 1,
                    overflow = TextOverflow.Ellipsis) },
                actions = {
                    //Edit
                    if(uiState.category.id != 0L) {
                        IconButton(
                            onClick = {
                                onEditClick(uiState.category.id) }) {
                            Icon(imageVector = Icons.TwoTone.Edit, contentDescription = null)
                        }
                        //Delete
                        IconButton(onClick = viewModel::displayDialog) {
                            Icon(imageVector = Icons.TwoTone.Delete, contentDescription = null)
                        }
                    }
                },
                navigationIcon = {
                    IconButton(
                        onClick = {
                             onNavigateUp()
                        }
                    ) {
                        Icon(
                            imageVector =
                                Icons.TwoTone.ArrowBack,
                            contentDescription = null
                        )
                    }
                }
            )
        }
    ){
        CategoryDisplayScreenContent(
            uiState = uiState,
            onTodoClick = {
                onTodoClick(it.id)
            },
            onTodoChecked = viewModel::checkTodo,
            modifier = Modifier.padding(it),
        )
        if(dialogState.shouldDisplayDialog) {
            ConfirmationDialog(
                onDismissRequest = viewModel::hideDialog,
                onConfirmation = {
                    viewModel.deleteCategory()
                    viewModel.hideDialog()
                    onNavigateToRoot()
                },
                dialogTitle = stringResource(R.string.confirm_loeschen),
                dialogText = stringResource(R.string.unsaved_category_changes),
                icon = Icons.TwoTone.Warning
            )
        }
    }
}

@Composable
private fun CategoryDisplayScreenContent(
    uiState: CategoryDisplayUiState,
    onTodoClick: (Todo) -> Unit,
    onTodoChecked: (Todo, Boolean) -> Unit,
    modifier: Modifier = Modifier,
) {
    Surface(
        modifier = modifier
    ) {
        if (uiState.todoList.isEmpty()) {
            NoTodosTile(
                categoryName = uiState.category.name,
                modifier = Modifier.fillMaxSize()
            )
        } else {
            LazyColumn(
                modifier = Modifier.fillMaxSize(),
                contentPadding = PaddingValues(MaterialTheme.spacing.small),
                verticalArrangement = Arrangement.spacedBy(MaterialTheme.spacing.small)
            ) {
                items(uiState.todoList) { todo ->
                    TodoTile(
                        onClick = onTodoClick,
                        onCheckboxClick = onTodoChecked,
                        todo = todo
                    )
                }
            }
        }
    }
}

        @Composable
        fun NoTodosTile(
            categoryName: String,
            modifier: Modifier = Modifier
        ) {
            Surface(
                modifier = modifier
            ) {
                Column(
                    verticalArrangement = Arrangement.spacedBy(
                        MaterialTheme.spacing.small,
                        Alignment.CenterVertically
                    ),
                    horizontalAlignment = Alignment.CenterHorizontally
                ) {
                    Icon(
                        imageVector = Icons.TwoTone.TaskAlt,
                        contentDescription = null,
                        tint = MaterialTheme.colorScheme.surfaceTint
                    )
                    Text(
                        text = "Keine Todos in ${categoryName}.",
                        color = MaterialTheme.colorScheme.surfaceTint
                    )
                }
            }
        }

