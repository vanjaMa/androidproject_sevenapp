package com.com.todoAppseven.presentation.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.com.todoAppseven.presentation.viewmodel.exceptions.InvalidUserNameException
import com.example.preferencesdatastore.di.annotation.DataStorePreferences
import com.example.preferencesdatastore.repository.PrefRepo
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import javax.inject.Inject
import kotlin.jvm.Throws

//zum halten von daten
//entweder string oder null
data class WelcomeScreenUIState(val username: String? = null)

@HiltViewModel
class WelcomeScreenViewModel @Inject constructor(
    @DataStorePreferences private val prefRepo: PrefRepo
): ViewModel(){

    private val _uiState : MutableStateFlow<WelcomeScreenUIState> = MutableStateFlow(
        WelcomeScreenUIState()
    )
    val uiState = _uiState.asStateFlow()

    fun updateUsername(username: String) {
        _uiState.update {
            if(username.isBlank()) it.copy(username = null)//bissl dumme funktion->ändern
            else it.copy(username = username)
        }
    }
    @Throws(InvalidUserNameException::class)
    fun saveUsername() {
        if (_uiState.value.isValid()) {
            viewModelScope.launch {
                _uiState.value.username?.let {prefRepo.setUsername(it)
                     }
            }
        }
        else throw InvalidUserNameException()
    }
}
fun WelcomeScreenUIState.isValid() = !this.username.isNullOrBlank()