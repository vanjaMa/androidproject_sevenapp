package com.com.todoAppseven.presentation.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.com.todoAppseven.data.di.annotation.RoomDataSource
import com.com.todoAppseven.data.model.relationships.CategoryMitTodos
import com.com.todoAppseven.data.repository.CategoryRepo
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.stateIn
import javax.inject.Inject

data class CategoriesScreenUiState(
    val categoryList: List<CategoryMitTodos>
    )

@HiltViewModel
class CategoriesScreenViewModel @Inject constructor(
    @RoomDataSource private val categoryRepo: CategoryRepo,
   //@RoomDataSource private val todoRepo: TodoRepo
) : ViewModel() {

    val uiState = categoryRepo.getAllCategoriesWithTodos().map {
        categoriesMitTodos -> CategoriesScreenUiState(
            categoryList = categoriesMitTodos
        )
    }.stateIn(
        viewModelScope,
   SharingStarted.WhileSubscribed(TIMEOUT_MILLIS),
        CategoriesScreenUiState(listOf())
    )

    companion object {
        val TIMEOUT_MILLIS = 5000L
    }

}