package com.com.todoAppseven.presentation.helpers

import com.com.todoAppseven.data.model.Todo

//erweiterungfunktion

//returnt denn fortschritt von der todo-liste
fun List<Todo>.progress(): Float {

    //Gesamtzahl der Todos
    val allTodosCount = size

//Anzahl der erledigten Todos
    val doneTodosCount = filter { it.isDone }.size

//Verhältnis von erledigten Todos zur Gesamtzahl der Todos zurück
    return doneTodosCount.toFloat()/allTodosCount.toFloat()
}