package com.com.todoAppseven.presentation.viewmodel.exceptions

import androidx.annotation.StringRes

abstract class SevenAppException(
    message: String,
    @StringRes open val displayMessage: Int? = null
) : Exception(message)