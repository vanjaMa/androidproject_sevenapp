package com.com.todoAppseven.presentation.viewmodel

import androidx.compose.ui.graphics.Color
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.viewModelScope
import com.com.todoAppseven.data.di.annotation.RoomDataSource
import com.com.todoAppseven.data.model.Category
import com.com.todoAppseven.data.model.ColorHolder
import com.com.todoAppseven.data.repository.CategoryRepo
import com.com.todoAppseven.presentation.helpers.toColorHolder
import com.com.todoAppseven.presentation.view.screens.CategoryEntryDestination
import com.com.todoAppseven.presentation.viewmodel.exceptions.InvalidCatNameException
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.filterNotNull
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import javax.inject.Inject
import kotlin.jvm.Throws


data class CategoryEntryScreenUiState(
    val categoryName: String? = null,
    val colorHolder: ColorHolder,
    val newEntry: Boolean,
)
@HiltViewModel
class CategoryEntryViewModel  @Inject constructor(
    savedStateHandle: SavedStateHandle,
    @RoomDataSource private val categoryRepo: CategoryRepo
): DialogViewModel() {

    private val categoryId: Long = savedStateHandle[CategoryEntryDestination.parameterName] ?: 0L

    private val _uiState =
        MutableStateFlow(
            CategoryEntryScreenUiState(
                categoryName = null,
                colorHolder = Color.Blue.toColorHolder(),
                newEntry = true
            )
        )
    val uiState = _uiState.asStateFlow()

    init {
        viewModelScope.launch {
            val category = categoryRepo
                .getCategoryById(categoryId).filterNotNull().first()

            _uiState.update {
                it.copy(
                    categoryName = if (categoryId == 0L) null else category.name,
                    colorHolder = if (categoryId == 0L) Color.Blue.toColorHolder() else category.color,
                    newEntry = categoryId == 0L
                )
            }
        }
    }

    fun updateCategoryName(newName: String) {
        _uiState.update {
            it.copy(
                categoryName = newName
            )
        }
    }

    fun updateCategoryColor(newColor: Color) {
        _uiState.update {
            it.copy(
                colorHolder = newColor.toColorHolder()
            )
        }
    }


    @Throws(InvalidCatNameException::class)
    fun saveCategory() {
        if (uiState.value.isValid()) {
            viewModelScope.launch {
                if (_uiState.value.newEntry) {
                    categoryRepo.addCategory(uiState.value.toCategory())
                } else {
                    categoryRepo.updateCategory(uiState.value.toCategory(categoryId))
                }
            }
        } else throw InvalidCatNameException()
    }
}
    private fun CategoryEntryScreenUiState.isValid(): Boolean {
        return !categoryName.isNullOrBlank()
    }
    @Throws(InvalidCatNameException::class)
    private fun CategoryEntryScreenUiState.toCategory(id: Long = 0) : Category {
        if (categoryName == null) throw InvalidCatNameException()
        return Category(
            id = id,
            name = categoryName,
            color = colorHolder
        )
    }








