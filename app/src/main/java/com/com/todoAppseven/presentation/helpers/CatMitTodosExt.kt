package com.com.todoAppseven.presentation.helpers

import com.com.todoAppseven.data.model.relationships.CategoryMitTodos

fun CategoryMitTodos.getCompletedTodosRatio(): Float {


    // Zählt die Anzahl der Aufgaben, die als erledigt markiert sind
    val doneTodosNum = this.todos.filter {

        it.isDone  // Filtert die Aufgaben, die als erledigt markiert sind
    }.size  // Gibt die Anzahl der erledigten Aufgaben zurück

//wenn es keine aufgaben gibt--> 0
    //// Andernfalls wird das Verhältnis
    // von erledigten Aufgaben zur Gesamtzahl der Aufgaben berechnet
    return if(todos.isEmpty()) 0.toFloat() else doneTodosNum.toFloat() / todos.size.toFloat()
}