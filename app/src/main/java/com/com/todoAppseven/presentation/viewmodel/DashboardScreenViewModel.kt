package com.com.todoAppseven.presentation.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.com.todoAppseven.data.di.annotation.RoomDataSource
import com.com.todoAppseven.data.model.Todo
import com.com.todoAppseven.data.model.relationships.CategoryMitTodos
import com.com.todoAppseven.data.repository.CategoryRepo
import com.com.todoAppseven.data.repository.TodoRepo
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.flow.stateIn
import kotlinx.coroutines.launch
import java.time.LocalDate
import java.time.LocalDateTime
import javax.inject.Inject

data class DashBoardScreenUiState(
    val categoryList: List<CategoryMitTodos>,
    val todaysTodos: List<Todo>,
    val uncategorizedTodos: List<Todo>
)

@HiltViewModel
class DashboardScreenViewModel @Inject constructor(
    @RoomDataSource val categoryRepo: CategoryRepo,
    @RoomDataSource val todoRepo: TodoRepo
) : ViewModel() {

    val uiState = categoryRepo.getAllCategoriesWithTodos()
        .combine(todoRepo.getAllTodos()) { categories, todos ->
            val todayTodos = todos.filter {
                it.dueDate == LocalDate.now()
            }
            val uncategorizedTodos = todos.filter {
                it.categoryId == null
            }
            DashBoardScreenUiState(categories, todayTodos, uncategorizedTodos)
        }.stateIn(
            viewModelScope,
          SharingStarted.WhileSubscribed(TIMEOUT_MILLIS),
            DashBoardScreenUiState(listOf(), listOf(), listOf())
        )

    //zum markieren
    fun checkTodo(todo: Todo, checked: Boolean) {
        viewModelScope.launch {
            if (checked) {
                todoRepo.removeTodo(todo)
            } else if (todo.isDone) {
                //todo unchecked
                todoRepo.updateTodo(todo.copy(isDone = false, completedOn = null))
            }
        }
    }
    companion object {
        val TIMEOUT_MILLIS = 5000L
    }
}