package com.com.todoAppseven.presentation.viewmodel

import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.viewModelScope
import com.com.todoAppseven.data.di.annotation.RoomDataSource
import com.com.todoAppseven.data.model.Category
import com.com.todoAppseven.data.model.Step
import com.com.todoAppseven.data.model.Todo
import com.com.todoAppseven.data.model.relationships.TodosMitSteps
import com.com.todoAppseven.data.repository.CategoryRepo
import com.com.todoAppseven.data.repository.StepRepo
import com.com.todoAppseven.data.repository.TodoRepo
import com.com.todoAppseven.presentation.view.screens.TodoEntryDestination
import com.com.todoAppseven.presentation.viewmodel.exceptions.InvalidDateException
import com.com.todoAppseven.presentation.viewmodel.exceptions.InvalidTodoNameException
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.filterNotNull
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.firstOrNull
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import java.time.LocalDate
import java.time.LocalDateTime
import javax.inject.Inject
import kotlin.jvm.Throws


data class TodoEntryUiState(
    val todoId: Long = 0L,
    val newEntry: Boolean,

    //editable
    val todoName: String = "",
    val todoDescription: String? = null,
    val dueDate: LocalDate = LocalDate.now(),
    val categoryId: Long? = null,


    val isDone: Boolean = false,
    val createdOn: LocalDateTime,
    val completedOn: LocalDateTime? = null,
    val selectedCategoryName: String? = null,

    val categoryList: List<Category> = listOf(),

    val isAddingNewStep: Boolean = false,
    val stepDescription: String? = null,
    private val _stepsList: MutableList<Step> = mutableListOf(),
) {
    val stepsList: List<Step> = _stepsList
}

@HiltViewModel
class TodoEntryViewModel @Inject constructor(
    savedStateHandle: SavedStateHandle,
    @RoomDataSource private val todoRepo: TodoRepo,
    @RoomDataSource private val stepRepo: StepRepo,
    @RoomDataSource private val categoryRepo: CategoryRepo
) : DialogViewModel() {

    private val todoId = savedStateHandle[TodoEntryDestination.parameterName] ?: 0L

    private val _uiState =
        MutableStateFlow(TodoEntryUiState(newEntry = true, createdOn = LocalDateTime.now()))
    val uiState = _uiState.asStateFlow()

    private val _datePickerDialogState = MutableStateFlow(DialogDisplayState(false))
    val datePickerDialogState = _datePickerDialogState.asStateFlow()

    init {
        viewModelScope.launch {
            refreshUiState()
        }
    }

    private suspend fun refreshUiState() {
        val categoryList = categoryRepo.getAllCategories().first()
        _uiState.update {
            it.copy(
                categoryList = categoryList,
            )
        }
        val todoWithSteps = todoRepo.getTodoWithStepsById(todoId).filterNotNull().firstOrNull()

        _uiState.update {
            it.copy(
                todoId = todoId,
                newEntry = todoWithSteps == null,
                todoName = todoWithSteps?.todo?.name ?: it.todoName,
                todoDescription = todoWithSteps?.todo?.description ?: it.todoDescription,
                dueDate = todoWithSteps?.todo?.dueDate ?: it.dueDate,
                categoryId = todoWithSteps?.todo?.categoryId ?: it.categoryId,
                isDone = todoWithSteps?.todo?.isDone ?: it.isDone,
                createdOn = todoWithSteps?.todo?.createdOn ?: it.createdOn,
                completedOn = todoWithSteps?.todo?.completedOn ?: it.completedOn,
                selectedCategoryName = todoWithSteps?.todo?.categoryId?.let { categoryId ->
                    categoryList.firstOrNull { category -> category.id == categoryId }?.name
                } ?: it.selectedCategoryName,
                isAddingNewStep = it.isAddingNewStep,
                stepDescription = it.stepDescription,
                _stepsList = todoWithSteps?.steps?.toMutableList() ?: it.stepsList.toMutableList(),
            )
        }
    }
    fun displayDatePickerDialog() {
        _datePickerDialogState.update {
            it.copy(shouldDisplayDialog = true)
        }
    }
    fun hideDatePickerDialog() {
        _datePickerDialogState.update {
            it.copy(shouldDisplayDialog = false)
        }
    }
    fun switchStepEntryMode(isActive: Boolean) = _uiState.update {
        it.copy(
            isAddingNewStep = isActive,
            stepDescription = null
        )
    }
    fun updateStepDescription(description: String) = _uiState.update {
        it.copy(
            stepDescription = description
        )
    }
    fun updateTodoName(todoName: String) = _uiState.update {
        it.copy(
            todoName = todoName
        )
    }
    fun updateTodoDescription(todoDescription: String) = _uiState.update {
        it.copy(
            todoDescription = todoDescription
        )
    }
    fun updateSelectedCategory(category: Category) = _uiState.update {
        it.copy(
            categoryId = category.id,
            selectedCategoryName = category.name
        )
    }
    @Throws(InvalidDateException::class)
    fun updateDueDate(date: LocalDate) {
        if (!date.isBefore(LocalDate.now())) {
            _uiState.update {
                it.copy(
                    dueDate = date
                )
            }
        } else throw InvalidDateException()
    }
    fun deleteStep(step: Step) {
        val newStepList = _uiState.value.stepsList.toMutableList()
        newStepList.remove(step)
        _uiState.update {
            it.copy(
                _stepsList = newStepList
            )
        }
        if(!_uiState.value.newEntry) {
            viewModelScope.launch {
                stepRepo.removeStep(step)
                refreshUiState()
            }
        }
    }
    fun addStep(stepDescription: String) {
        viewModelScope.launch {
            val newStepList = _uiState.value.stepsList.toMutableList()
            val step = Step (
                todoId = todoId,
                description = stepDescription,
                createdOn = LocalDateTime.now(),
                isDone = false
            )
            newStepList.add(step)
            _uiState.update {
                it.copy(
                    stepDescription = null,
                    _stepsList = newStepList
                )
            }
        }
    }
    fun addNewTodoWithSteps() {
        if(_uiState.value.isValid()) {
            viewModelScope.launch {
                val todoId = addNewTodo()
                addStepsForTodo(todoId)
            }
        } else throw InvalidTodoNameException()
    }

    fun updateTodoWithSteps() {
        if(_uiState.value.isValid()) {
            viewModelScope.launch {
                todoRepo.updateTodo(_uiState.value.toTodoWithSteps().todo)
                addStepsForTodo(todoId)
            }
        } else throw InvalidTodoNameException()
    }
    private suspend fun addStepsForTodo(todoId: Long) {
        _uiState.value.stepsList.forEach {
            stepRepo.addStep(
                it.copy(
                    todoId = todoId,
                    createdOn = LocalDateTime.now()
                )
            )
        }
    }
    private suspend fun addNewTodo(): Long {
        val todoToAdd = _uiState.value.toTodoWithSteps().todo
        return todoRepo.addTodo(todoToAdd)
    }
}
fun TodoEntryUiState.isValid() = todoName.isNotBlank()

fun TodoEntryUiState.toTodoWithSteps(): TodosMitSteps {
    if(isValid()) {
        return TodosMitSteps(
            todo = Todo(
                id = todoId,
                categoryId = categoryId,
                name = todoName,
                description = todoDescription,
                createdOn = createdOn,
                completedOn = completedOn,
                dueDate = dueDate,
                isDone = isDone
            ),
            steps = stepsList
        )
    } else throw InvalidTodoNameException()
}
